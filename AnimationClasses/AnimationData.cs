﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
namespace AnimationClasses
{
    public class AnimationData
    {
        public List<float> timeAxis = new List<float>();
        public Dictionary<int, List<Matrix>> bones = new Dictionary<int,List<Matrix>>();
    }
}
