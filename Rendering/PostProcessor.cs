﻿using LDEngine.Game.Scripts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LDEngine.Engine.Managers;

namespace LDEngine.Engine.Rendering
{
    class PostProcessor
    {
        float mousePosition = 0f;

        RenderTarget2D[] targets;
        GraphicsDevice device;

        Effect postProcesses;

        EffectTechnique bloom;
        EffectTechnique vignette;
        EffectTechnique rain;
        EffectTechnique fog;
        EffectTechnique outline;

        RenderTarget2D target, texture;
        Texture2D noise;
        Texture2D rainTex;
        Texture2D fogTex;

        public PostProcessor(RenderTarget2D[] targets, GraphicsDevice device)
        {
            this.targets = targets;
            this.device = device;
            postProcesses = ResourceManager.Instance.Load<Effect>("Effects/PostProcesses");
            noise = ResourceManager.Instance.Load<Texture2D>("Textures/noise");
            rainTex = ResourceManager.Instance.Load<Texture2D>("Textures/rain");
            fogTex = ResourceManager.Instance.Load<Texture2D>("Textures/fog");
            
            //Setup techniques
            bloom = postProcesses.Techniques["Bloom"];
            vignette = postProcesses.Techniques["Vignette"];
            rain = postProcesses.Techniques["Rain"];
            fog = postProcesses.Techniques["Fog"];
            outline = postProcesses.Techniques["Outline"];
            postProcesses.Parameters["inv_res"].SetValue(new Vector2(1f / targets[0].Width, 1f / targets[0].Height));
        }

        public void Begin()
        {
            FullScreenQuad.instance.ReadyBuffers();
        }
        public void Outline()
        {
            postProcesses.CurrentTechnique = outline;

            device.SetRenderTarget(targets[1]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[0]);
            postProcesses.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }
        public void Bloom(float threshold, float blurAmount)
        {
            postProcesses.CurrentTechnique = bloom;
            postProcesses.Parameters["noiseTex"].SetValue(noise);

            //extract brightness from 1 to 2
            device.SetRenderTarget(targets[2]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[1]);
            postProcesses.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            //horizontal blur from 2 to 3
            device.SetRenderTarget(targets[3]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[2]);

            //SetBlurEffectParameters(1.0f / (float)targets[1].Width, 0);
            postProcesses.Parameters["blurDirection"].SetValue(new Vector2(0, 1));
            postProcesses.CurrentTechnique.Passes[1].Apply();
            FullScreenQuad.instance.JustDraw();

            //vertical blur from 3 to 2
            device.SetRenderTarget(targets[0]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[3]);
            //SetBlurEffectParameters(0, 1.0f / (float)targets[1].Height);
            postProcesses.Parameters["blurDirection"].SetValue(new Vector2(1, 0));
            postProcesses.CurrentTechnique.Passes[1].Apply();
            FullScreenQuad.instance.JustDraw();

            //combine from 1 to 0
            device.SetRenderTarget(targets[2]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[1]);
            postProcesses.Parameters["bloomIntensity"].SetValue(PlayerController.Instance.bloomIntensity);
            postProcesses.Parameters["baseIntensity"].SetValue(PlayerController.Instance.baseBloomIntensity);
            postProcesses.Parameters["bloomSaturation"].SetValue(1.0f);
            postProcesses.Parameters["baseSaturation"].SetValue(0.85f);
            postProcesses.Parameters["bloomTex"].SetValue(targets[0]);

            postProcesses.CurrentTechnique.Passes[2].Apply();
            FullScreenQuad.instance.JustDraw();

        }
        public void Fog(Matrix vpMatrixInv, Vector4 offset0, Vector4 offset1, Vector3 camPosition, Vector3 fogColor0, Vector3 fogColor1, Vector2 invHeight, Vector2 distForce)
        {
            postProcesses.CurrentTechnique = fog;
            postProcesses.Parameters["vpMatrixInv"].SetValue(vpMatrixInv);
            postProcesses.Parameters["offset0"].SetValue(offset0);
            postProcesses.Parameters["offset1"].SetValue(offset1);
            postProcesses.Parameters["camPosition"].SetValue(camPosition);
            postProcesses.Parameters["fogColor0"].SetValue(fogColor0);
            postProcesses.Parameters["fogColor1"].SetValue(fogColor1);
            postProcesses.Parameters["invHeight"].SetValue(invHeight);
            postProcesses.Parameters["distForce"].SetValue(distForce);
            postProcesses.Parameters["noiseTex"].SetValue(fogTex);

            device.SetRenderTarget(targets[1]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[2]);
            postProcesses.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }
        public void Rain(Vector4 tiloff)
        {
            postProcesses.CurrentTechnique = rain;
            mousePosition += Input.mousePositionDelta.X * 0.01f;
            postProcesses.Parameters["noiseTex"].SetValue(rainTex);
            postProcesses.Parameters["tiloff"].SetValue(tiloff + new Vector4(0f, 0f, mousePosition, 0f));
            device.SetRenderTarget(targets[2]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[1]);
            postProcesses.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }
        public void Vignette()
        {
            postProcesses.CurrentTechnique = vignette;

            if (Time.timeSinceLevelLoad < 3f) {
                postProcesses.Parameters["screenFadeIntensity"].SetValue(1 - Time.timeSinceLevelLoad / 3f);     // initial fade
            }
            else if (BossDummy.animationTimer > 11f) {
                postProcesses.Parameters["screenFadeIntensity"].SetValue(1);     // outro fade
            }

            postProcesses.Parameters["vignetteIntensity"].SetValue((1f - ((float)PlayerController.Instance.health / PlayerController.Instance.maxHealth)) * 0.2f);
            device.SetRenderTarget(targets[0]);
            postProcesses.Parameters["shadedTex"].SetValue(targets[2]);
            postProcesses.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }

        /// <summary>
        /// Computes sample weightings and texture coordinate offsets
        /// for one pass of a separable gaussian blur filter.
        /// </summary>
        void SetBlurEffectParameters(float dx, float dy)
        {
            // Look up the sample weight and offset effect parameters.
            EffectParameter weightsParameter = postProcesses.Parameters["SampleWeights"];
            EffectParameter offsetsParameter = postProcesses.Parameters["SampleOffsets"];

            // Look up how many samples our gaussian blur effect supports.
            int sampleCount = weightsParameter.Elements.Count;

            // Create temporary arrays for computing our filter settings.
            float[] sampleWeights = new float[sampleCount];
            Vector2[] sampleOffsets = new Vector2[sampleCount];

            // The first sample always has a zero offset.
            sampleWeights[0] = ComputeGaussian(0);
            sampleOffsets[0] = new Vector2(0);

            // Maintain a sum of all the weighting values.
            float totalWeights = sampleWeights[0];

            // Add pairs of additional sample taps, positioned
            // along a line in both directions from the center.
            for (int i = 0; i < sampleCount / 2; i++)
            {
                // Store weights for the positive and negative taps.
                float weight = ComputeGaussian(i + 1);

                sampleWeights[i * 2 + 1] = weight;
                sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                // To get the maximum amount of blurring from a limited number of
                // pixel shader samples, we take advantage of the bilinear filtering
                // hardware inside the texture fetch unit. If we position our texture
                // coordinates exactly halfway between two texels, the filtering unit
                // will average them for us, giving two samples for the price of one.
                // This allows us to step in units of two texels per sample, rather
                // than just one at a time. The 1.5 offset kicks things off by
                // positioning us nicely in between two texels.
                float sampleOffset = i * 2 + 1.5f;

                Vector2 delta = new Vector2(dx, dy) * sampleOffset;

                // Store texture coordinate offsets for the positive and negative taps.
                sampleOffsets[i * 2 + 1] = delta;
                sampleOffsets[i * 2 + 2] = -delta;
            }

            // Normalize the list of sample weightings, so they will always sum to one.
            for (int i = 0; i < sampleWeights.Length; i++)
            {
                sampleWeights[i] /= totalWeights;
            }

            // Tell the effect about our new filter settings.
            weightsParameter.SetValue(sampleWeights);
            offsetsParameter.SetValue(sampleOffsets);
        }

        /// <summary>
        /// Evaluates a single point on the gaussian falloff curve.
        /// Used for setting up the blur filter weightings.
        /// </summary>
        float ComputeGaussian(float n)
        {
            float theta = 100.0f; //blur amount

            return (float)((1.0 / Math.Sqrt(2 * Math.PI * theta)) *
                           Math.Exp(-(n * n) / (2 * theta * theta)));
        }
    }
}
