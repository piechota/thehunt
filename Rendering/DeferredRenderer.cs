﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LDEngine.Engine.Collections;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using LDEngine.Engine.Managers;

namespace LDEngine.Engine.Rendering
{
    class DeferredRenderer : Renderer
    {
        Effect skyboxEffect;
        Effect gBufferEffect;
        Effect simpleEffect;
        Effect directLights;
        Effect blendCubeMap;

        GraphicsDevice graphicsDevice;
        RenderTargetBinding[] gBuffer;
        RenderTarget2D scene0;

        Matrix vpMatrix;
        Matrix vMatrix;
        Matrix pMatrixInvert;
        Matrix vMatrixInvert;
        Matrix vpMatrixInvert;
        Vector3 camPosition;

        Model skybox;
        Texture2D skybox_texture;

        RenderTargetCube difEnvCube;
        RenderTargetCube specEnvCube;
        Texture2D intTexture;

        DepthStencilState dssParticle;
        PostProcessor postProcessor;

        int drawModelsProfilerID;
        #region Singleton
        private static DeferredRenderer _instance;
        public static DeferredRenderer Instance
        {
            get { return _instance ?? (_instance = new DeferredRenderer()); }
        }
        #endregion

        public void SetupDeferredRenderer(GraphicsDevice graphicsDevice, ContentManager content, int width, int height)
        {
            drawModelsProfilerID = Profiler.Instance.CreateTimer("DrawModels");

            this.graphicsDevice = graphicsDevice;
            FullScreenQuad.instance.SetFullScreenQuad(graphicsDevice, content);

            skyboxEffect = ResourceManager.Instance.Load<Effect>("Effects/Deferred/SkyboxDeferredEffect");
            skyboxEffect.CurrentTechnique = skyboxEffect.Techniques[0];
            gBufferEffect = ResourceManager.Instance.Load<Effect>("Effects/Deferred/GBufferEffect");
            gBufferEffect.CurrentTechnique = gBufferEffect.Techniques[0];
            simpleEffect = ResourceManager.Instance.Load<Effect>("Effects/SimpleEffect");
            simpleEffect.CurrentTechnique = simpleEffect.Techniques[0];

            directLights = ResourceManager.Instance.Load<Effect>("Effects/Deferred/Lights/Lights");
            directLights.CurrentTechnique = directLights.Techniques[0];

            blendCubeMap = ResourceManager.Instance.Load<Effect>("Effects/CubeMaps/BlendCubeMap");
            blendCubeMap.CurrentTechnique = blendCubeMap.Techniques[0];

            difEnvCube = new RenderTargetCube(graphicsDevice, 128, false, SurfaceFormat.Dxt3, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            specEnvCube = new RenderTargetCube(graphicsDevice, 256, false, SurfaceFormat.Dxt3, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);

            gBuffer = new RenderTargetBinding[4];
            gBuffer[0] = new RenderTargetBinding(new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8, 0, RenderTargetUsage.PreserveContents));
            gBuffer[1] = new RenderTargetBinding(new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents));
            gBuffer[2] = new RenderTargetBinding(new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents));
            gBuffer[3] = new RenderTargetBinding(new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents));

            scene0 = new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);

            RenderTarget2D t0 = new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            RenderTarget2D t1 = new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            RenderTarget2D t2 = new RenderTarget2D(graphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);

            RenderTarget2D[] targets  = new RenderTarget2D[4]{scene0, t0, t1, t2};


            postProcessor = new PostProcessor(targets, graphicsDevice);

            skybox = ResourceManager.Instance.Load<Model>("Skybox");
            skybox_texture = ResourceManager.Instance.Load<Texture2D>("skybox_texture");

            MakeSkyboxGI(content);
            //LoadEnvSpecMap(content);

            intTexture = ResourceManager.Instance.Load<Texture2D>("EnvCubemaps/int");

            dssParticle = new DepthStencilState();
            dssParticle.DepthBufferEnable = true;
            dssParticle.DepthBufferWriteEnable = false;
            dssParticle.StencilEnable = true;
            dssParticle.ReferenceStencil = 1;
            dssParticle.StencilFunction = CompareFunction.NotEqual;
            dssParticle.StencilPass = StencilOperation.Keep;

            simpleEffect.Parameters["diffuseMap"].SetValue(scene0);
            directLights.Parameters["albedoTex"].SetValue(gBuffer[0].RenderTarget);
            directLights.Parameters["normal_metalnessTex"].SetValue(gBuffer[1].RenderTarget);
            directLights.Parameters["emissive_roughnessTex"].SetValue(gBuffer[2].RenderTarget);
            directLights.Parameters["depthTex"].SetValue(gBuffer[3].RenderTarget);

            directLights.Parameters["diffEnvCube"].SetValue(difEnvCube);
            directLights.Parameters["specEnvCube"].SetValue(specEnvCube);
            directLights.Parameters["intText"].SetValue(intTexture);
        }
        public override void BlendCubeMaps(KeyValuePair<float, ReflectionProbe>[] reflectionProbes, Camera cam)
        {
            float skyboxWeight = MathHelper.Max(0f, 1f - (reflectionProbes[0].Key + reflectionProbes[1].Key + reflectionProbes[2].Key + reflectionProbes[3].Key));

            blendCubeMap.Parameters["blend1Level"].SetValue(reflectionProbes[0].Key);
            blendCubeMap.Parameters["blend2Level"].SetValue(reflectionProbes[1].Key);
            blendCubeMap.Parameters["blend3Level"].SetValue(reflectionProbes[2].Key);
            blendCubeMap.Parameters["blend4Level"].SetValue(reflectionProbes[3].Key);

            FullScreenQuad.instance.ReadyBuffers();
            Matrix view;
            Vector3 reflCamera;

            if (reflectionProbes[0].Value != null)
            {
                blendCubeMap.Parameters["blend1CUBE"].SetValue(reflectionProbes[0].Value.difEnvCube);

                reflCamera = cam.transform.position;
                reflCamera.Y += 2f * ((reflectionProbes[0].Value.boxPosition.Y - reflCamera.Y) - Math.Abs(reflectionProbes[0].Value.worldToLocal.Up.Y));

                blendCubeMap.Parameters["worldToLocal1"].SetValue(reflectionProbes[0].Value.worldToLocal);
                blendCubeMap.Parameters["reflCameraWS1"].SetValue(reflCamera);
                blendCubeMap.Parameters["reflCameraLS1"].SetValue(Vector3.Transform(reflCamera, reflectionProbes[0].Value.worldToLocal));
                blendCubeMap.Parameters["cubeMapPos1"].SetValue(reflectionProbes[0].Value.transform.position);
            }
            if (reflectionProbes[1].Value != null)
            {
                blendCubeMap.Parameters["blend2CUBE"].SetValue(reflectionProbes[1].Value.difEnvCube);

                reflCamera = cam.transform.position;
                reflCamera.Y += 2f * ((reflectionProbes[1].Value.boxPosition.Y - reflCamera.Y) - Math.Abs(reflectionProbes[1].Value.worldToLocal.Up.Y));

                blendCubeMap.Parameters["worldToLocal2"].SetValue(reflectionProbes[1].Value.worldToLocal);
                blendCubeMap.Parameters["reflCameraWS2"].SetValue(reflCamera);
                blendCubeMap.Parameters["reflCameraLS2"].SetValue(Vector3.Transform(reflCamera, reflectionProbes[1].Value.worldToLocal));
                blendCubeMap.Parameters["cubeMapPos2"].SetValue(reflectionProbes[1].Value.transform.position);
            }
            if (reflectionProbes[2].Value != null)
            {
                blendCubeMap.Parameters["blend3CUBE"].SetValue(reflectionProbes[2].Value.difEnvCube);

                reflCamera = cam.transform.position;
                reflCamera.Y += 2f * ((reflectionProbes[2].Value.boxPosition.Y - reflCamera.Y) - Math.Abs(reflectionProbes[2].Value.worldToLocal.Up.Y));

                blendCubeMap.Parameters["worldToLocal3"].SetValue(reflectionProbes[2].Value.worldToLocal);
                blendCubeMap.Parameters["reflCameraWS3"].SetValue(reflCamera);
                blendCubeMap.Parameters["reflCameraLS3"].SetValue(Vector3.Transform(reflCamera, reflectionProbes[2].Value.worldToLocal));
                blendCubeMap.Parameters["cubeMapPos3"].SetValue(reflectionProbes[2].Value.transform.position);
            }
            if (reflectionProbes[3].Value != null)
            {
                blendCubeMap.Parameters["blend4CUBE"].SetValue(reflectionProbes[3].Value.difEnvCube);

                reflCamera = cam.transform.position;
                reflCamera.Y += 2f * ((reflectionProbes[3].Value.boxPosition.Y - reflCamera.Y) - Math.Abs(reflectionProbes[3].Value.worldToLocal.Up.Y));

                blendCubeMap.Parameters["worldToLocal4"].SetValue(reflectionProbes[3].Value.worldToLocal);
                blendCubeMap.Parameters["reflCameraWS4"].SetValue(reflCamera);
                blendCubeMap.Parameters["reflCameraLS4"].SetValue(Vector3.Transform(reflCamera, reflectionProbes[3].Value.worldToLocal));
                blendCubeMap.Parameters["cubeMapPos4"].SetValue(reflectionProbes[3].Value.transform.position);
            }

            blendCubeMap.Parameters["skyboxDiffLevel"].SetValue(skyboxWeight);
            blendCubeMap.Parameters["skyboxSpecLevel"].SetValue(0f);
            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.PositiveZ);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.NegativeZ);
            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();


            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.PositiveX);
            view = new Matrix(
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.NegativeX);
            view = new Matrix(
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                -1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.PositiveY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.NegativeY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, -1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            if (reflectionProbes[0].Value != null)
            {
                blendCubeMap.Parameters["blend1CUBE"].SetValue(reflectionProbes[0].Value.specEnvCube);
            }
            if (reflectionProbes[1].Value != null)
            {
                blendCubeMap.Parameters["blend2CUBE"].SetValue(reflectionProbes[1].Value.specEnvCube);
            }
            if (reflectionProbes[2].Value != null)
            {
                blendCubeMap.Parameters["blend3CUBE"].SetValue(reflectionProbes[2].Value.specEnvCube);
            }
            if (reflectionProbes[3].Value != null)
            {
                blendCubeMap.Parameters["blend4CUBE"].SetValue(reflectionProbes[3].Value.specEnvCube);
            }
            blendCubeMap.Parameters["skyboxDiffLevel"].SetValue(0f);
            blendCubeMap.Parameters["skyboxSpecLevel"].SetValue(skyboxWeight);
            graphicsDevice.SetRenderTarget(specEnvCube, CubeMapFace.PositiveZ);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(specEnvCube, CubeMapFace.NegativeZ);
            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();


            graphicsDevice.SetRenderTarget(specEnvCube, CubeMapFace.PositiveX);
            view = new Matrix(
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(specEnvCube, CubeMapFace.NegativeX);
            view = new Matrix(
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                -1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(specEnvCube, CubeMapFace.PositiveY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(specEnvCube, CubeMapFace.NegativeY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, -1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            blendCubeMap.Parameters["dirMatrix"].SetValue(view);
            blendCubeMap.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }
        protected void MakeSkyboxGI(ContentManager content)
        {
            Matrix projection = Matrix.CreateOrthographic(2f, 2f, 0.01f, 10f);
            Matrix view;
            RenderTargetCube skyboxCube = new RenderTargetCube(graphicsDevice, 256, false, SurfaceFormat.Dxt3, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            Effect diffuseGI = ResourceManager.Instance.Load<Effect>("Effects/CubeMaps/DiffuseGI");
            diffuseGI.CurrentTechnique = diffuseGI.Techniques[0];
            skyboxEffect.Parameters["skyboxMap"].SetValue(skybox_texture);

            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 0f, 0f, 1f
                );
            graphicsDevice.SetRenderTarget(skyboxCube, CubeMapFace.PositiveZ);
            skyboxEffect.Parameters["vpMatrix"].SetValue(view * projection);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f
                );
            graphicsDevice.SetRenderTarget(skyboxCube, CubeMapFace.NegativeZ);
            skyboxEffect.Parameters["vpMatrix"].SetValue(view * projection);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }

            view = new Matrix(
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                -1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            graphicsDevice.SetRenderTarget(skyboxCube, CubeMapFace.PositiveX);
            skyboxEffect.Parameters["vpMatrix"].SetValue(view * projection);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }

            view = new Matrix(
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            graphicsDevice.SetRenderTarget(skyboxCube, CubeMapFace.NegativeX);
            skyboxEffect.Parameters["vpMatrix"].SetValue(view * projection);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }

            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, -1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            graphicsDevice.SetRenderTarget(skyboxCube, CubeMapFace.PositiveY);
            skyboxEffect.Parameters["vpMatrix"].SetValue(view * projection);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }
            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            graphicsDevice.SetRenderTarget(skyboxCube, CubeMapFace.NegativeY);
            skyboxEffect.Parameters["vpMatrix"].SetValue(view * projection);
            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }

            FullScreenQuad.instance.ReadyBuffers();
            RenderTargetCube skyboxEnvCube = new RenderTargetCube(graphicsDevice, 128, false, SurfaceFormat.Dxt3, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            graphicsDevice.SetRenderTarget(skyboxEnvCube, CubeMapFace.PositiveZ);
            diffuseGI.Parameters["skyboxCubeTex"].SetValue(skyboxCube);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(skyboxEnvCube, CubeMapFace.NegativeZ);
            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();


            graphicsDevice.SetRenderTarget(skyboxEnvCube, CubeMapFace.PositiveX);
            view = new Matrix(
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(skyboxEnvCube, CubeMapFace.NegativeX);
            view = new Matrix(
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                -1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(skyboxEnvCube, CubeMapFace.PositiveY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(skyboxEnvCube, CubeMapFace.NegativeY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, -1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(null);
            directLights.Parameters["skyboxDiffCUBE"].SetValue(skyboxEnvCube);
            directLights.Parameters["skyboxSpecCUBE"].SetValue(skyboxCube);
            blendCubeMap.Parameters["skyboxDiffCUBE"].SetValue(skyboxEnvCube);
            blendCubeMap.Parameters["skyboxSpecCUBE"].SetValue(skyboxCube);
        }

        public override void DrawSkybox(Camera cam)
        {
            graphicsDevice.SetRenderTargets(gBuffer);
            graphicsDevice.Clear(Color.Transparent);

            Matrix rot = Matrix.Invert(Matrix.CreateFromQuaternion(cam.transform.rotation));

            skyboxEffect.Parameters["vpMatrix"].SetValue(Transform.matrixLH_X * rot * cam.projectionMatrix);

            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }
        }
        public override void DrawSkybox(Matrix camView, Matrix camProj)
        {
            graphicsDevice.SetRenderTargets(gBuffer);
            graphicsDevice.Clear(Color.Transparent);

            skyboxEffect.Parameters["vpMatrix"].SetValue(Transform.matrixLH_X * camView * camProj);

            foreach (ModelMesh mesh in skybox.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    skyboxEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }
        }
        public override void DrawModels(MeshRenderersList meshRenderers)
        {
            Profiler.Instance.StartTimer(drawModelsProfilerID);
            foreach (MeshRenderer renderer in meshRenderers)
            {                
                if (renderer.enabled /*TODO: culling && renderer.IsVisible()*/) {
                    Matrix[] boneTransforms = new Matrix[renderer.model.Bones.Count];
                    renderer.model.CopyAbsoluteBoneTransformsTo(boneTransforms);
                    foreach (ModelMesh mesh in renderer.model.Meshes)
                    {
                        foreach (ModelMeshPart part in mesh.MeshParts)
                        {
                            graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                            graphicsDevice.Indices = part.IndexBuffer;

                            Matrix wvpMatrix = boneTransforms[mesh.ParentBone.Index] * renderer.gameObject.transform.worldMatrix * vpMatrix;
                            //Matrix wvpMatrix = mesh.ParentBone.Transform * renderer.gameObject.transform.worldMatrix * vpMatrix;

                            gBufferEffect.Parameters["wvpMatrix"].SetValue(wvpMatrix);
                            gBufferEffect.Parameters["wvMatrix"].SetValue(boneTransforms[mesh.ParentBone.Index] * renderer.gameObject.transform.worldMatrix * vMatrix);

                            gBufferEffect.CurrentTechnique.Passes[0].Apply();

                            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                        }
                    }
                }
            }
            Profiler.Instance.StopTimer(drawModelsProfilerID);
        }

        public override void InitModelsDrawing(Matrix camView, Matrix camProj)
        {
            graphicsDevice.Clear(ClearOptions.DepthBuffer, Color.Black, 1f, 0);

            vpMatrix = camView * camProj;
            vMatrix = camView;
            pMatrixInvert = Matrix.Invert(camProj);
            vMatrixInvert = Matrix.Invert(camView);
            vpMatrixInvert = pMatrixInvert * vMatrixInvert;
            camPosition = Vector3.Zero;

        }
        public override void InitModelsDrawing(Camera cam)
        {
            graphicsDevice.Clear(ClearOptions.DepthBuffer, Color.Black, 1f, 0);

            vpMatrix = cam.viewMatrix * cam.projectionMatrix;
            vMatrix = cam.viewMatrix;
            pMatrixInvert = Matrix.Invert(cam.projectionMatrix);
            vMatrixInvert = Matrix.Invert(cam.viewMatrix);
            vpMatrixInvert = pMatrixInvert * vMatrixInvert;
            camPosition = cam.transform.position;
        }

        public override void SetMaterial(Material mat)
        {
            gBufferEffect.Parameters["diffuseTex"].SetValue(mat.diffuse);
            gBufferEffect.Parameters["normalTex"].SetValue(mat.normal);
            gBufferEffect.Parameters["metalnessTex"].SetValue(mat.metalness);
            gBufferEffect.Parameters["roughnessTex"].SetValue(mat.roughness);
            gBufferEffect.Parameters["emissiveTex"].SetValue(mat.emissiv);
            gBufferEffect.Parameters["uvOT"].SetValue(new Vector4(mat.offset, mat.tiling.X, mat.tiling.Y));
            gBufferEffect.Parameters["emissiveIntensity"].SetValue(mat.emissiveIntensity);
        }

        public override void DrawLights(LightsCollection lights)
        {
            FullScreenQuad.instance.ReadyBuffers();

            directLights.Parameters["pMatrixInvert"].SetValue(pMatrixInvert);
            directLights.Parameters["vMatrixInvert"].SetValue(vMatrixInvert);

            directLights.CurrentTechnique = directLights.Techniques["Skybox"];
            directLights.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.BlendState = BlendState.Additive;
            EffectParameter light;

            const int pointLightCount = 6;

            List<Light> lightsList = lights.GetLightsByType(Light.Type.directional);
            if (lightsList.Count > 0)
            {
                light = directLights.Parameters["directionalLight"];
                directLights.CurrentTechnique = directLights.Techniques["Directional"];

                foreach (Light d in lightsList)
                {
                    light.StructureMembers["forward"].SetValue(Vector3.TransformNormal(d.position, vMatrix));
                    light.StructureMembers["color"].SetValue(d.color.ToVector3());
                    light.StructureMembers["intensity"].SetValue(d.intensity);

                    directLights.CurrentTechnique.Passes[0].Apply();
                    FullScreenQuad.instance.JustDraw();
                }
            }

            lightsList = lights.GetLightsByType(Light.Type.point);

            if (lightsList.Count > 0)
            {
                directLights.CurrentTechnique = directLights.Techniques["Point"];
                int pLightsCount = lightsList.Count;
                int lightN = 0;
                string lightName;
                foreach (Light p in lightsList)
                {
                    if (p.intensity == 0f || p.radius == 0f || (p.gameObject.isStatic && !p.IsVisible(new BoundingFrustum(vpMatrix))))
                        continue;
                    Matrix tmp = p.transform.worldMatrix;
                    if (lightN == pointLightCount)
                    {
                        lightN = 0;
                        directLights.CurrentTechnique.Passes[0].Apply();
                        FullScreenQuad.instance.JustDraw();
                    }

                    lightName = "pLight" + lightN.ToString();
                    light = directLights.Parameters[lightName];
                    light.StructureMembers["position"].SetValue(Vector3.Transform(p.position, vMatrix));
                    light.StructureMembers["color"].SetValue(p.color.ToVector3());
                    light.StructureMembers["intensity"].SetValue(p.intensity);
                    light.StructureMembers["radius"].SetValue(p.radius);
                    directLights.Parameters["pL" + lightN.ToString() + "i"].SetValue(true);

                    ++lightN;
                }

                if (lightN != pointLightCount)
                {
                    for (; lightN < pointLightCount; ++lightN)
                    {
                        directLights.Parameters["pL" + lightN.ToString() + "i"].SetValue(false);
                    }
                }
                directLights.CurrentTechnique.Passes[0].Apply();
                FullScreenQuad.instance.JustDraw();
            }
            directLights.CurrentTechnique = directLights.Techniques["Indirect"];
            directLights.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }
        public override void DrawLightsWithoutIn(LightsCollection lights)
        {
            FullScreenQuad.instance.ReadyBuffers();

            directLights.Parameters["pMatrixInvert"].SetValue(pMatrixInvert);
            directLights.Parameters["vMatrixInvert"].SetValue(vMatrixInvert);

            directLights.CurrentTechnique = directLights.Techniques["Skybox"];
            directLights.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.BlendState = BlendState.Additive;
            EffectParameter light;

            const int pointLightCount = 6;

            List<Light> lightsList = lights.GetLightsByType(Light.Type.directional);
            if (lightsList.Count > 0)
            {
                light = directLights.Parameters["directionalLight"];
                directLights.CurrentTechnique = directLights.Techniques["Directional"];

                foreach (Light d in lightsList)
                {
                    light.StructureMembers["forward"].SetValue(Vector3.TransformNormal(d.position, vMatrix));
                    light.StructureMembers["color"].SetValue(d.color.ToVector3());
                    light.StructureMembers["intensity"].SetValue(d.intensity);

                    directLights.CurrentTechnique.Passes[0].Apply();
                    FullScreenQuad.instance.JustDraw();
                }
            }
            lightsList = lights.GetLightsByType(Light.Type.point);
            if (lightsList.Count > 0)
            {
                directLights.CurrentTechnique = directLights.Techniques["Point"];
                int pLightsCount = lightsList.Count;
                int lightN = 0;
                string lightName;
                foreach (Light p in lightsList)
                {
                    if (p.intensity == 0f || p.radius == 0f)
                        continue;

                    Matrix tmp = p.transform.worldMatrix;

                    if (lightN == pointLightCount)
                    {
                        lightN = 0;
                        directLights.CurrentTechnique.Passes[0].Apply();
                        FullScreenQuad.instance.JustDraw();
                    }

                    lightName = "pLight" + lightN.ToString();
                    light = directLights.Parameters[lightName];
                    light.StructureMembers["position"].SetValue(Vector3.Transform(p.position, vMatrix));
                    light.StructureMembers["color"].SetValue(p.color.ToVector3());
                    light.StructureMembers["intensity"].SetValue(p.intensity);
                    light.StructureMembers["radius"].SetValue(p.radius);
                    directLights.Parameters["pL" + lightN.ToString() + "i"].SetValue(true);

                    ++lightN;
                }

                if (lightN != pointLightCount)
                {
                    for (; lightN < pointLightCount; ++lightN)
                    {
                        directLights.Parameters["pL" + lightN.ToString() + "i"].SetValue(false);
                    }
                }
                directLights.CurrentTechnique.Passes[0].Apply();
                FullScreenQuad.instance.JustDraw();
            }
            directLights.CurrentTechnique = directLights.Techniques["Emissive"];
            directLights.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
        }
        public override void DrawScene()
        {
            simpleEffect.Parameters["gammaCorrect"].SetValue(gammaCorrect);
            simpleEffect.CurrentTechnique.Passes[0].Apply();

            FullScreenQuad.instance.Draw();
        }

        public override void InitDrawing()
        {
            graphicsDevice.SetRenderTarget(scene0);
            graphicsDevice.Clear(Color.Transparent);
        }

        public override void InitParticleDraw()
        {
            graphicsDevice.DepthStencilState = dssParticle;
        }
        public override void InitGunDraw()
        {
            graphicsDevice.Clear(ClearOptions.DepthBuffer, Color.Transparent, 1f, 0);
            //graphicsDevice.DepthStencilState = dssGun;
        }
        public override void DrawGun()
        {
            gBufferEffect.Parameters["diffuseTex"].SetValue(gunR.material.diffuse);
            gBufferEffect.Parameters["normalTex"].SetValue(gunR.material.normal);
            gBufferEffect.Parameters["metalnessTex"].SetValue(gunR.material.metalness);
            gBufferEffect.Parameters["roughnessTex"].SetValue(gunR.material.roughness);
            gBufferEffect.Parameters["emissiveTex"].SetValue(gunR.material.emissiv);
            gBufferEffect.Parameters["emissiveIntensity"].SetValue(gunR.material.emissiveIntensity);

            Matrix[] boneTransforms = new Matrix[gunR.model.Bones.Count];
            gunR.model.CopyAbsoluteBoneTransformsTo(boneTransforms);
            foreach (ModelMesh mesh in gunR.model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    Matrix wvpMatrix = boneTransforms[mesh.ParentBone.Index] * gunR.gameObject.transform.worldMatrix * vpMatrix;
                    //Matrix wvpMatrix = mesh.ParentBone.Transform * renderer.gameObject.transform.worldMatrix * vpMatrix;

                    gBufferEffect.Parameters["wvpMatrix"].SetValue(wvpMatrix);
                    gBufferEffect.Parameters["wvMatrix"].SetValue(boneTransforms[mesh.ParentBone.Index] * gunR.gameObject.transform.worldMatrix * vMatrix);

                    gBufferEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }

            gBufferEffect.Parameters["diffuseTex"].SetValue(gunL.material.diffuse);
            gBufferEffect.Parameters["normalTex"].SetValue(gunL.material.normal);
            gBufferEffect.Parameters["metalnessTex"].SetValue(gunL.material.metalness);
            gBufferEffect.Parameters["roughnessTex"].SetValue(gunL.material.roughness);
            gBufferEffect.Parameters["emissiveTex"].SetValue(gunL.material.emissiv);
            gBufferEffect.Parameters["emissiveIntensity"].SetValue(gunL.material.emissiveIntensity);

            boneTransforms = new Matrix[gunL.model.Bones.Count];
            gunL.model.CopyAbsoluteBoneTransformsTo(boneTransforms);
            foreach (ModelMesh mesh in gunL.model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    graphicsDevice.Indices = part.IndexBuffer;

                    Matrix wvpMatrix = boneTransforms[mesh.ParentBone.Index] * gunL.gameObject.transform.worldMatrix * vpMatrix;
                    //Matrix wvpMatrix = mesh.ParentBone.Transform * renderer.gameObject.transform.worldMatrix * vpMatrix;

                    gBufferEffect.Parameters["wvpMatrix"].SetValue(wvpMatrix);
                    gBufferEffect.Parameters["wvMatrix"].SetValue(boneTransforms[mesh.ParentBone.Index] * gunL.gameObject.transform.worldMatrix * vMatrix);

                    gBufferEffect.CurrentTechnique.Passes[0].Apply();

                    graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                }
            }
        }

        bool isFog = false;
        public override void DrawPostProcesses()
        {
            postProcessor.Begin();
            postProcessor.Outline();
            postProcessor.Bloom(0.4f, 1.0f);
            postProcessor.Fog(
                vpMatrixInvert,
                new Vector4(
                    0.005f,
                    0.005f,
                    Time.time * 0.02f,
                    Time.time * 0.02f),
                new Vector4(
                    0.03f,
                    0.03f,
                    Time.time * -0.005f,
                    Time.time * -0.005f),
                camPosition,
                Vector3.One * 0.2f,
                Vector3.One * 0.05f,
                new Vector2(1f / 10f, 1f / 0.1f),
                new Vector2(0.045f, -1f));

            postProcessor.Rain(new Vector4(3f, 1.8f, Time.time * -0.9f, Time.time * -7.1f));
            postProcessor.Vignette();
        }   
    }
}
