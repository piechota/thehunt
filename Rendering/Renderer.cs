﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using LDEngine.Engine.Collections;

namespace LDEngine.Engine.Rendering
{
    class Renderer
    {
        public float gammaCorrect = 0.9f;
        public MeshRenderer gunR;
        public MeshRenderer gunL;
        public virtual void DrawModels(MeshRenderersList meshRenderers)
        {

        }

        public virtual void DrawSkybox(Camera cam)
        {

        }
        public virtual void DrawSkybox(Matrix camView, Matrix camProj)
        {

        }

        public virtual void DrawShadows(Collections.MeshRenderersList meshRenderers)
        {

        }

        public virtual void SetMaterial(Material mat)
        {

        }

        public virtual void DrawScene()
        {

        }

        public virtual void InitModelsDrawing(Camera cam)
        {

        }
        public virtual void InitModelsDrawing(Matrix camView, Matrix camProj)
        {

        }

        public virtual void InitShadowsDrawing()
        {

        }

        public virtual void InitDrawing()
        {

        }
        public virtual void DrawLights(Collections.LightsCollection lights) { }
        public virtual void DrawLightsWithoutIn(Collections.LightsCollection lights) { }
        public virtual void InitParticleDraw() { }
        public virtual void DrawParticle() { }
        public virtual void InitGunDraw() { }
        public virtual void DrawGun() { }

        public virtual void DrawPostProcesses() { }

        public virtual void BlendCubeMaps(KeyValuePair<float, ReflectionProbe>[] reflectionProbes, Camera cam) { }
    }
}
