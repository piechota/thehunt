﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LDEngine.Engine;
using LDEngine.Engine.Managers;
using LDEngine.Engine.Collections;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LDEngine.Engine.Rendering
{
    class ReflectionProbe : Component
    {
        public RenderTargetCube difEnvCube;
        public RenderTargetCube specEnvCube;

        public float radius;
        public float innerRadius;
        public Matrix worldToLocal;
        public Vector3 boxPosition;
        public ReflectionProbe()
        {
            DrawingManager.Instance.reflectionProbes.Add(this);
        }

        private void DrawSide(
            Renderer renderer, 
            RenderingList materialsList, 
            LightsCollection lights, 
            RenderTargetCube target, 
            CubeMapFace face, 
            GraphicsDevice device, 
            Matrix sideMatrix,
            Matrix rotateMatrix)
        {
            Matrix proj = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90f), 1f, 0.01f, 1000f);

            device.BlendState = BlendState.Opaque;
            device.DepthStencilState = DepthStencilState.None;

            //graphicsDevice.SetRenderTargets(gBuffer);
            //graphicsDevice.Clear(Color.Transparent);
            renderer.DrawSkybox(rotateMatrix, proj);
            device.DepthStencilState = DepthStencilState.Default;

            //graphicsDevice.Clear(ClearOptions.DepthBuffer, Color.Black, 1f, 0);
            renderer.InitModelsDrawing(sideMatrix, proj);

            int materialCount = materialsList.Count;
            for (int i = 0; i < materialCount; ++i)
            {
                if (materialsList[i].staticRenderers.Count > 0)
                {
                    //draw static only
                    renderer.SetMaterial(materialsList[i]);
                    renderer.DrawModels(materialsList[i].staticRenderers);
                }
            }
            device.DepthStencilState = DepthStencilState.Default;
            //graphicsDevice.SetRenderTarget(scene0);
            //graphicsDevice.Clear(Color.Transparent);
            renderer.InitDrawing();
            //graphicsDevice.BlendState = bsLights;
            renderer.DrawLightsWithoutIn(lights);

            device.SetRenderTarget(target, face);
            device.Clear(Color.Black);
            renderer.DrawScene();
        }
        public void DrawProbe(Renderer renderer, RenderingList materialsList, LightsCollection lights, GraphicsDevice graphicsDevice)
        {
            if (transform.GetChildren().Count > 0)
            {
                boxPosition = transform.GetChildren()[0].position;
                worldToLocal = Matrix.Invert(transform.GetChildren()[0].worldMatrix);
            }
            //worldToLocal = Matrix.Invert
            //  (
            //  new Matrix
            //      (
            //        1f, 0f, 0f, 0f,
            //        0f, 1f, 0f, 0f,
            //        0f, 0f, 1f, 0f,
            //        0f, 0f, 0f, 1f
            //      ) *
            //      Matrix.CreateTranslation(boxPosition)
            //  );
            Matrix invTranslation = Matrix.Invert(Matrix.CreateTranslation(transform.position));

            Matrix view;
            specEnvCube = new RenderTargetCube(graphicsDevice, 256, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            Effect diffuseGI = ResourceManager.Instance.Load<Effect>("Effects/CubeMaps/DiffuseGI");
            diffuseGI.CurrentTechnique = diffuseGI.Techniques[0];

            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f
                );
            DrawSide(renderer, materialsList, lights, specEnvCube, CubeMapFace.NegativeZ, graphicsDevice, invTranslation * view, view);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 0f, 0f, 1f
                );
            DrawSide(renderer, materialsList, lights, specEnvCube, CubeMapFace.PositiveZ, graphicsDevice, invTranslation * view, view);

            view = new Matrix(
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            DrawSide(renderer, materialsList, lights, specEnvCube, CubeMapFace.NegativeX, graphicsDevice, invTranslation * view, view);

            view = new Matrix(
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                -1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            DrawSide(renderer, materialsList, lights, specEnvCube, CubeMapFace.PositiveX, graphicsDevice, invTranslation * view, view);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, -1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            DrawSide(renderer, materialsList, lights, specEnvCube, CubeMapFace.PositiveY, graphicsDevice, invTranslation * view, view);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            DrawSide(renderer, materialsList, lights, specEnvCube, CubeMapFace.NegativeY, graphicsDevice, invTranslation * view, view);



            FullScreenQuad.instance.ReadyBuffers();
            difEnvCube = new RenderTargetCube(graphicsDevice, 128, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);
            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.PositiveZ);
            diffuseGI.Parameters["skyboxCubeTex"].SetValue(specEnvCube);

            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.NegativeZ);
            view = new Matrix(
                -1f, 0f, 0f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();


            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.PositiveX);
            view = new Matrix(
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.NegativeX);
            view = new Matrix(
                0f, 0f, 1f, 0f,
                0f, 1f, 0f, 0f,
                -1f, 0f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.PositiveY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, -1f, 0f,
                0f, 1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();
            graphicsDevice.SetRenderTarget(difEnvCube, CubeMapFace.NegativeY);
            view = new Matrix(
                1f, 0f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, -1f, 0f, 0f,
                0f, 0f, 0f, 1f
                );
            diffuseGI.Parameters["mat"].SetValue(view);
            diffuseGI.CurrentTechnique.Passes[0].Apply();
            FullScreenQuad.instance.JustDraw();

            Material mat = new Material()
            {
                diffuse = ResourceManager.Instance.Load<Texture2D>("Materials/Pure Mirror/Base_Color"),
                normal = ResourceManager.Instance.Load<Texture2D>("Materials/Pure Mirror/Normal"),
                metalness = ResourceManager.Instance.Load<Texture2D>("Materials/Pure Mirror/Metallic"),
                roughness = ResourceManager.Instance.Load<Texture2D>("Materials/Pure Mirror/Roughness")
            };
            //MeshRenderer.ModelViewer("Models/sphere", mat, transform.position, 1f);

            //MeshRenderer.ModelViewer("Models/box", mat, transform.GetChildren()[0].gameObject, 1f);
        }
    }
}
