﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace LDEngine.Engine.Rendering
{
    class FullScreenQuad
    {
        static protected FullScreenQuad _instance;
        static public FullScreenQuad instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FullScreenQuad();

                return _instance;
            }
        }

        protected VertexBuffer vertex;
        protected IndexBuffer index;

        protected GraphicsDevice graphicsDevice;

        protected FullScreenQuad()
        { }

        public void SetFullScreenQuad(GraphicsDevice graphicsDevice, ContentManager content)
        {
            this.graphicsDevice = graphicsDevice;

            VertexPositionTexture[] vertices = 
            {
                new VertexPositionTexture(new Vector3( 1, -1, 0), new Vector2(1, 1)),
                new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1)),
                new VertexPositionTexture(new Vector3(-1,  1, 0), new Vector2(0, 0)),
                new VertexPositionTexture(new Vector3( 1,  1, 0), new Vector2(1, 0))
            };

            vertex = new VertexBuffer(graphicsDevice, VertexPositionTexture.VertexDeclaration, vertices.Length, BufferUsage.None);
            vertex.SetData<VertexPositionTexture>(vertices);

            ushort[] indices = { 0, 1, 2, 2, 3, 0 };

            index = new IndexBuffer(graphicsDevice, IndexElementSize.SixteenBits, indices.Length, BufferUsage.None);
            index.SetData<ushort>(indices);
        }

        public void Draw()
        {
            graphicsDevice.SetVertexBuffer(vertex);
            graphicsDevice.Indices = index;
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
        }

        public void ReadyBuffers()
        {
            graphicsDevice.SetVertexBuffer(vertex);
            graphicsDevice.Indices = index;
        }

        public void JustDraw()
        {
            graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 2);
        }

    }
}
