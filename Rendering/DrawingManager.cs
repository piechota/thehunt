﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using LDEngine.Engine.Collections;
using LDEngine.Engine.Managers;
using LDEngine.Game.Scripts;

namespace LDEngine.Engine.Rendering
{
    class DrawingManager
    {
        public RenderingList materialsList { get; private set; }
        CullingSystem culling;

        public Renderer renderer;
        List<Camera> cameras;
        LightsCollection lights;
        public ParticleSystem particleSystem;
        public ReflectionProbesList reflectionProbes;
        public GraphicsDevice device;

        public Material defaultMaterial;

        public bool isDeferred { get; protected set; }

        private DrawingManager()
        {
            lights = new LightsCollection();
            materialsList = new RenderingList();
            //renderer = new XNARenderer();
            cameras = new List<Camera>();
            particleSystem = new ParticleSystem();
            reflectionProbes = new ReflectionProbesList();
        }

        public void Add(Light light)
        {
            lights.Add(light);        
        }

        public void Remove(Light light)
        {
            lights.Remove(light);
        }
        public List<Light> GetStaticPointLights()
        {
            List<Light> result = new List<Light>();
            foreach(Light item in lights.GetLightsByType(Light.Type.point))
            {
                if (item.gameObject.isStatic == true)
                    result.Add(item);
            }
            return result;
        }
        public void GoDeffered(GraphicsDevice device, ContentManager content)
        {
            this.device = device;
            renderer = DeferredRenderer.Instance;
            DeferredRenderer.Instance.SetupDeferredRenderer(device, content, 1024, 1024);
            isDeferred = true;
            particleSystem.Init(device);

            defaultMaterial = new Material
            {
                diffuse = ResourceManager.Instance.Load<Texture2D>("Materials/Textures/white"),
                normal = ResourceManager.Instance.Load<Texture2D>("Materials/Textures/normal"),
                metalness = ResourceManager.Instance.Load<Texture2D>("Materials/Textures/black"),
                roughness = ResourceManager.Instance.Load<Texture2D>("Materials/Textures/black"),
            };
            culling = new CullingSystem();
            Add(defaultMaterial);
        }
        public void Add(VisibilityArea visibilityArea)
        {
            culling.Add(visibilityArea);
        }
        internal void CalculateCulling()
        {
            culling.CalculateVisibility();
        }
        public void Add(ParticleEmitter emitter)
        {
            particleSystem.Add(emitter);
        }

        public void Add(Camera cam)
        {
            cameras.Add(cam);
        }

        public void RemoveCamera(Camera cam)
        {
            cameras.Remove(cam);
        }

        private static DrawingManager _instance;

        public static DrawingManager Instance
        {
            get { return _instance ?? (_instance = new DrawingManager()); }
        }

        public void MakeStatic(MeshRenderer mr)
        {
            mr.material.staticRenderers.Add(mr);
            mr.material.renderers.Remove(mr);
        }

        public void Add(Material mat)
        {
            materialsList.Add(mat);
        }

        public void Remove(Material mat)
        {
            materialsList.Remove(mat);
        }

        public void Remove(ParticleEmitter emitter)
        {
            particleSystem.emitters.Remove(emitter);
        }

        KeyValuePair<float, ReflectionProbe>[] GetReflectionProbesWeight(Camera cam)
        {
            KeyValuePair<float, ReflectionProbe>[] probes = new KeyValuePair<float, ReflectionProbe>[4];
            probes[0] = new KeyValuePair<float, ReflectionProbe>(0f, null);
            probes[1] = new KeyValuePair<float, ReflectionProbe>(0f, null);
            probes[2] = new KeyValuePair<float, ReflectionProbe>(0f, null);
            probes[3] = new KeyValuePair<float, ReflectionProbe>(0f, null);
            float weight;
            float weightSum = 0f;
            foreach(ReflectionProbe rp in reflectionProbes)
            {
                weight = MathHelper.Clamp(
                    1f - 
                    (
                    (Vector3.Distance(cam.transform.position, rp.transform.position) - rp.innerRadius) / 
                    (rp.radius - rp.innerRadius)
                    )
                    , 0f, 1f);
                for(int i = 0; i < 4; ++i)
                {
                    if(weight > probes[i].Key)
                    {
                        KeyValuePair<float, ReflectionProbe> tmp0 = probes[i];
                        KeyValuePair<float, ReflectionProbe> tmp1;
                        probes[i] = new KeyValuePair<float, ReflectionProbe>(weight, rp);
                        weightSum -= tmp0.Key;
                        weightSum += probes[i].Key;
                        for(int j = i+1; j < 4; ++j)
                        {
                            tmp1 = probes[j];
                            probes[j] = tmp0;
                            tmp0 = tmp1;
                        }
                        break;
                    }
                }
            }
            //weightSum = weightSum == 0f ? 1f : weightSum;
            //for (int i = 0; i < 4; ++i)
            //    probes[i] = new KeyValuePair<float, ReflectionProbe>(probes[i].Key / weightSum, probes[i].Value);

            return probes;
        }

        public void Draw()
        {
            //TODO: culling
            //culling.UpdateVisibility();
            int cameraCount = cameras.Count;
            int materialCount = materialsList.Count;
            for (int c = 0; c < cameraCount; ++c)
            {
                if (cameras[c].enabled)
                {
                    device.BlendState = BlendState.Opaque;
                    device.DepthStencilState = DepthStencilState.None;
                    renderer.BlendCubeMaps(GetReflectionProbesWeight(cameras[c]), cameras[c]);

                    //graphicsDevice.SetRenderTargets(gBuffer);
                    //graphicsDevice.Clear(Color.Transparent);
                    renderer.DrawSkybox(cameras[c]);
                    device.DepthStencilState = DepthStencilState.Default;
                    
                    //graphicsDevice.Clear(ClearOptions.DepthBuffer, Color.Black, 1f, 0);
                    renderer.InitModelsDrawing(cameras[c]);



                    for (int i = 0; i < materialCount; ++i)
                    {
                        //draw dynamic
                        if (materialsList[i].renderers.Count > 0)
                        {
                            renderer.SetMaterial(materialsList[i]);
                            renderer.DrawModels(materialsList[i].renderers);
                            //draw static
                            renderer.DrawModels(materialsList[i].staticRenderers);
                        }
                        else if (materialsList[i].staticRenderers.Count > 0)
                        {
                            //draw static only
                            renderer.SetMaterial(materialsList[i]);
                            renderer.DrawModels(materialsList[i].staticRenderers);
                        }
                    }
                    device.BlendState = BlendState.Additive;
                    //graphicsDevice.SetRenderTargets(gBuffer);
                    //graphicsDevice.DepthStencilState = dssParticle;
                    renderer.InitParticleDraw();

                    particleSystem.DrawDeferred(cameras[c]);

                    device.DepthStencilState = DepthStencilState.Default;
                    device.BlendState = BlendState.Opaque;
                    //clear depth
                    renderer.InitGunDraw();
                    renderer.DrawGun();

                    //graphicsDevice.SetRenderTarget(scene0);
                    //graphicsDevice.Clear(Color.Transparent);
                    renderer.InitDrawing();
                    //graphicsDevice.BlendState = bsLights;
                    renderer.DrawLights(lights);

                    device.BlendState = BlendState.Opaque;
                    renderer.DrawPostProcesses();

                    device.SetRenderTarget(null);
                    device.Clear(Color.Black);
                    renderer.DrawScene();

                    //device.BlendState = BlendState.Additive;
                    //renderer.DrawParticle();


                    //Draw particles

                    //device.SetVertexBuffers();
                }
            }
            DebugGraphics();
        }

        public Material GetMaterialByName(string name)
        {
            foreach (Material item in materialsList)
            {
                if (item.name == name)
                    return item;
            }

            return null;
        }

        public void DebugGraphics()
        {
            uint verticesCount = 0;
            uint objectCount = 0;
            foreach (Material item in materialsList)
            {
                foreach (MeshRenderer renderer in item.renderers)
                {
                    ++objectCount;
                    foreach (ModelMesh mesh in renderer.model.Meshes)
                    {
                        foreach (ModelMeshPart part in mesh.MeshParts)
                        {
                            verticesCount += (uint)part.NumVertices;
                        }
                    }
                }
            }
            
        }

        public void CreateReflectionProbes()
        {
            foreach (ReflectionProbe rp in reflectionProbes)
                rp.DrawProbe(renderer, materialsList, lights, device);
        }
    }
}
