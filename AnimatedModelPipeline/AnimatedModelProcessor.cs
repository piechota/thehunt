using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

using AnimationClasses;

using TInput = System.String;
using TOutput = System.String;

namespace AnimatedModelPipeline
{
    /// <summary>
    /// This class will be instantiated by the XNA Framework Content Pipeline
    /// to apply custom processing to content data, converting an object of
    /// type TInput to TOutput. The input and output types may be the same if
    /// the processor wishes to alter data without changing its type.
    ///
    /// This should be part of a Content Pipeline Extension Library project.
    ///
    /// display name for this processor.
    /// </summary>
    /// 
    [ContentProcessor(DisplayName = "AnimatedModel")]
    public class AnimatedModelProcessor : ModelProcessor
    {
        static Matrix matrixLH_X = new Matrix(
               -1f, 0f, 0f, 0f,
               0f, 1f, 0f, 0f,
               0f, 0f, 1f, 0f,
               0f, 0f, 0f, 1f);

        AnimationData animationData = new AnimationData();
        List<Matrix> keyframes;
        Dictionary<String, NodeContent> bones = new Dictionary<TOutput, NodeContent>();
        public override ModelContent Process(NodeContent input, ContentProcessorContext context)
        {
            //System.Diagnostics.Debugger.Launch();
            AddBones(input);
            ModelContent model = base.Process(input, context);
            NodeContent tmp = null;
            foreach(ModelBoneContent bone in model.Bones)
            {
                if (!bones.ContainsKey(bone.Name))
                    continue;

                tmp = bones[bone.Name];

                keyframes = new List<Matrix>();

                foreach (AnimationContent ac in tmp.Animations.Values)
                {
                    foreach (AnimationChannel ach in ac.Channels.Values)
                    {
                        foreach (AnimationKeyframe ak in ach)
                        {
                            if (!animationData.timeAxis.Contains((float)ak.Time.TotalSeconds))
                                animationData.timeAxis.Add((float)ak.Time.TotalSeconds);

                            keyframes.Add(ak.Transform);
                        }
                    }
                }
                animationData.bones[bone.Index] = keyframes;
            }
            model.Tag = animationData;
            return model;
        }

        protected void AddBones(NodeContent node)
        {
            if (!bones.ContainsKey(node.Name)) 
                bones.Add(node.Name, node);
            foreach (NodeContent nc in node.Children)
                AddBones(nc);
        }
    }
}