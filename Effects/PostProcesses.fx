sampler albedo : register(s4);
sampler normal_metalness : register(s5);
sampler emissive_roughness : register(s6);
sampler depth : register(s7);

texture bloomTex : register(t9);
sampler bloomSampler  : register(s9) 
= sampler_state
{
	texture = < bloomTex >;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture shadedTex : register(t10);
sampler shaded  : register(s10) 
= sampler_state
{
	texture = < shadedTex >;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture noiseTex : register(t11);
sampler noise  : register(s11) 
= sampler_state
{
	texture = < noiseTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

static const float3 byte_to_float = float3(1.0f, 1.0f / 256.f, 1.0f / (256.f * 256.f));
//color_to_depth = dot(tex2D(depth, input.uv).rgb, byte_to_float)

struct VSIO
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

VSIO VS(VSIO input)
{
	VSIO output;

	output.position = input.position;
	output.uv = input.uv;

	return output;
}

//OUTLINE
static const int samples = 16;
static const float radius = 5.f;
static const float strength = 3.f;
static const float dec = strength / 8.f;
static const float2 sample_sphere[samples] = {
	normalize(float2(0.5381, 0.1856)), normalize(float2(0.1379, 0.2486)),
	normalize(float2(0.3371, 0.5679)), normalize(float2(-0.6999, -0.0451)),
	normalize(float2(0.0689, -0.1598)), normalize(float2(0.0560, 0.0069)),
	normalize(float2(-0.0146, 0.1402)), normalize(float2(0.0100, -0.1924)),
	normalize(float2(-0.3577, -0.5301)), normalize(float2(-0.3169, 0.1063)),
	normalize(float2(0.0103, -0.5869)), normalize(float2(-0.0897, -0.4940)),
	normalize(float2(0.7119, -0.0154)), normalize(float2(-0.0533, 0.0596)),
	normalize(float2(0.0352, -0.0631)), normalize(float2(-0.4776, 0.2847))
};
//BLOOM

float2 inv_res;
static const float bloomThreshold = 0.4f;
float4 extractBrightness(VSIO input) : COLOR0
{
	float4 color = tex2D(shaded, input.uv);

	return saturate((color - bloomThreshold) / (1.f - bloomThreshold));
}

#define SAMPLE_COUNT 15

float2 SampleOffsets[SAMPLE_COUNT];
float SampleWeights[SAMPLE_COUNT];

float2 blurDirection;

static const float iters = 40;
static const float invIters = 1.f / iters;
static const float sampleJump = 0.0010f;
static const float halfIters = iters / 2;
float4 cBlur(VSIO input) : COLOR0
{
	float3 color = 0.f;

	[unroll]
	for (float i = -halfIters; i < halfIters; ++i)
		color += tex2D(shaded, reflect(mad(tex2D(noise, input.uv * i * 0.015625f).xy, 2.f, -1.f), blurDirection) * inv_res * 200.f + input.uv).rgb;

	return float4(color * invIters, 1.f);
}

float bloomIntensity;
float baseIntensity;

float bloomSaturation;
float baseSaturation;

float4 AdjustSaturation(float4 color, float saturation)
{
	// The constants 0.3, 0.59, and 0.11 are chosen because the
	// human eye is more sensitive to green light, and less to blue.

	float grey = dot(color, float3(0.3f, 0.5f, 0.11f));
	color.b *= 0.9f;

	return lerp(grey, color, saturation);
}

float4 bloomCombine(VSIO input) : COLOR0
{
	float4 bloom = tex2D(bloomSampler, input.uv);
	float4 base = tex2D(shaded, input.uv);

	bloom = AdjustSaturation(bloom, bloomSaturation) * bloomIntensity;
	base = AdjustSaturation(base, baseSaturation) * baseIntensity;

	base *= (1.f - saturate(bloom));

	return base + bloom;
}
//FOG
float4x4 vpMatrixInv;
float4 offset0;
float4 offset1;
float3 camPosition;
float3 fogColor0;
float3 fogColor1;
float2 invHeight;
float2 distForce;
float4 fog(VSIO input) : COLOR0
{
	float3 depth_test = tex2D(depth, input.uv).rgb;
	if (dot(depth_test, 1.f) >= 3.f) return tex2D(shaded, input.uv);

	float4 position = 1.f;
	position.xy = input.uv;
	position.y = 1.f - position.y;
	position.xy = mad(position.xy, 2.f, -1.f);
	position.z = dot(depth_test, byte_to_float);
	position = mul(position, vpMatrixInv);
	position.xyz /= position.w;

	float leng = length(position.xyz - camPosition);
	float distance = distForce.x > 0.f ? leng * distForce.x : 1.f;
	if (position.y * invHeight.x > 1.f) return tex2D(shaded, input.uv);
	float2 uv = position.xz + float2(0.f, position.y);

	float g = mad(-position.y, invHeight.x, 1.f);
	float4 fog0 = mad(
		float4(fogColor0, 1.f) *	//fog color
		tex2D(noise, mad(uv, offset0.xy, offset0.zw)),	//noise
		distance *	//distance fade
		g,	//height
		tex2D(shaded, input.uv));	//shaded texture

	uv = position.xz - (position.xz - camPosition.xz)*((1.f / invHeight.y) - position.y) / (camPosition.y - position.y);
	distance = distForce.y > 0.f ? leng * distForce.y : 1.f;
	if (position.y * invHeight.y > 1.f) return fog0;

	g = mad(-position.y, invHeight.y, 1.f);
	return mad(
		float4(fogColor1, 1.f) *	//fog color
		tex2D(noise, mad(uv, offset1.xy, offset1.zw)),	//noise
		distance *	//distance fade
		g,	//height
		fog0);	//shaded texture
}
//RAIN
float4 tiloff;
float4 rain(VSIO input) : COLOR0
{
	float4 rain = tex2D(noise, mad(input.uv, tiloff.xy, tiloff.zw));
	return float4(rain.xyz, 1.f) * rain.w * rain.w * 0.25f + tex2D(shaded, input.uv);
}


float4 edgeDetected(VSIO input) : COLOR0
{
	float depth0 = dot(tex2D(depth, input.uv).rgb, byte_to_float);
	float3 normal0 = normalize(mad(tex2D(normal_metalness, input.uv).xyz, 2.f, -1.f));

		float test = 1.f;
	float2 radius_pixel = inv_res * (radius / depth0);
		float2 uv;
	[unroll]
	for (int i = 0; i < 8; ++i)
	{
		uv = mad(sample_sphere[i], radius_pixel, input.uv);
		test -=
			dec *
			step(0.f, depth0 - dot(tex2D(depth, uv).rgb, byte_to_float)) *
			(1.f - dot(normalize(mad(tex2D(normal_metalness, uv).xyz, 2.f, -1.f)), normal0));
	}
	test = saturate(test);
	return float4(test, test, test, 1.f) * tex2D(shaded, input.uv);
}
//VIGNETTE
float vignetteIntensity;
float screenFadeIntensity;
float4 vignette(VSIO input) : COLOR0
{
	half2 coords = mad(input.uv, 2.f, -1.f);

	return tex2D(shaded, input.uv) - (dot(coords, coords) * vignetteIntensity * float4(0, 1, 1, 1))			// red vignette
	- float4(screenFadeIntensity, screenFadeIntensity, screenFadeIntensity, screenFadeIntensity);			// screen fade
}
technique Outline
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 edgeDetected();
	}
};
technique Bloom
{
	pass extract
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 extractBrightness();
	}

	pass blur
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 cBlur();
	}

	pass combine
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 bloomCombine();
	}
};
technique Fog
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 fog();
	}
};
technique Rain
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 rain();
	}
};
technique Vignette
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 vignette();
	}
};


