texture skyboxCubeTex : register(t9);
samplerCUBE skyboxCube : register(s9)
= sampler_state
{
	texture = < skyboxCubeTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

float4x4 mat;
struct VSIO
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

VSIO VS(VSIO input)
{
	VSIO output;

	output.position = input.position;
	output.uv = input.uv;

	return output;
}

float4 PS(VSIO input) : COLOR0
{
	const float inc = 2.f / 16.f;
	float3 color = 0.f;

	float3 normal = float3(input.uv.x, 1.f-input.uv.y, 1.0f);
	normal = mad(normal, 2.0f, -1.0f);
	normal = normalize(normal);
	
	float dot_sum = 0.f;

	for (float x = -1.0f; x <= 1.0f; x += inc)
	{
		for (float y = -1.0f; y <= 1.0f; y += inc)
		{
			float3 rayOffset = float3(x * 0.5f, y * 0.5f, 0.0f);
			float3 ray = normal + rayOffset;
			ray = normalize(ray);

			float NoR = max(0.0f, dot(ray, normal));
			dot_sum += NoR;

			color += texCUBE(skyboxCube, mul(float4(ray, 0.f), mat)).rgb * NoR;
		}
	}

	color /= dot_sum;
	return float4(color, 1.f);
}
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};