texture skyboxDiffCUBE : register(t0);
samplerCUBE skyboxDiff  : register(s0)
= sampler_state
{
	texture = < skyboxDiffCUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture skyboxSpecCUBE : register(t1);
samplerCUBE skyboxSpec  : register(s1)
= sampler_state
{
	texture = < skyboxSpecCUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture blend1CUBE : register(t9);
samplerCUBE blend1  : register(s9)
= sampler_state
{
	texture = < blend1CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend2CUBE : register(t10);
samplerCUBE blend2  : register(s10)
= sampler_state
{
	texture = < blend2CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend3CUBE : register(t11);
samplerCUBE blend3  : register(s11)
= sampler_state
{
	texture = < blend3CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend4CUBE : register(t12);
samplerCUBE blend4  : register(s12)
= sampler_state
{
	texture = < blend1CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

float4x4 dirMatrix;
float skyboxDiffLevel;
float skyboxSpecLevel;
float blend1Level;
float blend2Level;
float blend3Level;
float blend4Level;

float4x4 worldToLocal1;
float4x4 worldToLocal2;
float4x4 worldToLocal3;
float4x4 worldToLocal4;

float3 reflCameraWS1;
float3 reflCameraWS2;
float3 reflCameraWS3;
float3 reflCameraWS4;

float3 reflCameraLS1;
float3 reflCameraLS2;
float3 reflCameraLS3;
float3 reflCameraLS4;

float3 cubeMapPos1;
float3 cubeMapPos2;
float3 cubeMapPos3;
float3 cubeMapPos4;

struct PSI
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

PSI VS(PSI input)
{
	PSI output;
	output.position = input.position;
	output.uv = input.uv;
	return output;
}

half4 PS(PSI input) : COLOR0
{
	static const half3 unitary = 1.f;
	half3 dir = half3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dirMatrix).xyz);
	half3 color = texCUBE(skyboxDiff, dir).xyz * skyboxDiffLevel;
	color += texCUBE(skyboxSpec, dir).xyz * skyboxSpecLevel;

	half3 dirLS = mul(dir, (half3x3)worldToLocal1);
	half3 furthestPlane = max((unitary - reflCameraLS1) / dirLS, (-unitary - reflCameraLS1) / dirLS);
	float distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	half3 intersectPositionWS = reflCameraWS1 + dir * distance;
	color += texCUBE(blend1, intersectPositionWS - cubeMapPos1).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max((unitary - reflCameraLS2) / dirLS, (-unitary - reflCameraLS2) / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	color += texCUBE(blend2, intersectPositionWS - cubeMapPos2).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max((unitary - reflCameraLS3) / dirLS, (-unitary - reflCameraLS3) / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	color += texCUBE(blend3, intersectPositionWS - cubeMapPos3).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max((unitary - reflCameraLS4) / dirLS, (-unitary - reflCameraLS4) / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	color += texCUBE(blend4, intersectPositionWS - cubeMapPos4).xyz * blend4Level;

	return half4(color, 1.f);
}

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};