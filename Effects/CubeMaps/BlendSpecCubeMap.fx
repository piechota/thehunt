texture skyboxSpecCUBE : register(t0);
samplerCUBE skyboxSpec  : register(s0)
= sampler_state
{
	texture = < skyboxSpecCUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend1CUBE : register(t9);
samplerCUBE blend1  : register(s9)
= sampler_state
{
	texture = < blend1CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend2CUBE : register(t10);
samplerCUBE blend2  : register(s10)
= sampler_state
{
	texture = < blend2CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend3CUBE : register(t11);
samplerCUBE blend3  : register(s11)
= sampler_state
{
	texture = < blend3CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture blend4CUBE : register(t12);
samplerCUBE blend4  : register(s12)
= sampler_state
{
	texture = < blend1CUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

float4x4 dir1Matrix;
float4x4 dir2Matrix;
float4x4 dir3Matrix;
float4x4 dir4Matrix;

float4x4 worldToLocal1;
float4x4 worldToLocal2;
float4x4 worldToLocal3;
float4x4 worldToLocal4;

float3 reflCameraWS1;
float3 reflCameraWS2;
float3 reflCameraWS3;
float3 reflCameraWS4;

float3 reflCameraLS1;
float3 reflCameraLS2;
float3 reflCameraLS3;
float3 reflCameraLS4;

float3 cubeMapPos1;
float3 cubeMapPos2;
float3 cubeMapPos3;
float3 cubeMapPos4;

float mipmapLevel;
float skyboxSpecLevel;
float blend1Level;
float blend2Level;
float blend3Level;
float blend4Level;

struct PSI
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

PSI VS(PSI input)
{
	PSI output;
	output.position = input.position;
	output.uv = input.uv;
	return output;
}

struct PSO
{
	half4 side1 : COLOR0;
	half4 side2 : COLOR1;
	half4 side3 : COLOR2;
	half4 side4 : COLOR3;
};

PSO PS0(PSI input) : COLOR0
{
	PSO output;

	half3 unitary1 = 1.f - reflCameraLS1;
	half3 unitary2 = 1.f - reflCameraLS2;
	half3 unitary3 = 1.f - reflCameraLS3;
	half3 unitary4 = 1.f - reflCameraLS4;

	half3 negUnitary1 = -1.f - reflCameraLS1;
	half3 negUnitary2 = -1.f - reflCameraLS2;
	half3 negUnitary3 = -1.f - reflCameraLS3;
	half3 negUnitary4 = -1.f - reflCameraLS4;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	half3 dir = half3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir.xyz, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dir1Matrix).xyz);
	output.side1 = half4(texCUBElod(skyboxSpec, half4(dir, mipmapLevel)).xyz * skyboxSpecLevel, 1.f);

	half3 dirLS = mul(dir, (half3x3)worldToLocal1);
	half3 furthestPlane = max(unitary1 / dirLS, negUnitary1 / dirLS);
	float distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	half3 intersectPositionWS = reflCameraWS1 + dir * distance;
	output.side1.xyz += texCUBElod(blend1, half4(intersectPositionWS - cubeMapPos1, mipmapLevel)).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max(unitary2 / dirLS, negUnitary2 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	output.side1.xyz += texCUBElod(blend2, half4(intersectPositionWS - cubeMapPos2, mipmapLevel)).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max(unitary3 / dirLS, negUnitary3 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	output.side1.xyz += texCUBElod(blend3, half4(intersectPositionWS - cubeMapPos3, mipmapLevel)).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max(unitary4 / dirLS, negUnitary4 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	output.side1.xyz += texCUBElod(blend4, half4(intersectPositionWS - cubeMapPos4, mipmapLevel)).xyz * blend4Level;

	///////////////////////////////////////////////////////////////////////////////////////////
	dir = half3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dir2Matrix).xyz);
	output.side2 = half4(texCUBElod(skyboxSpec, half4(dir, mipmapLevel)).xyz * skyboxSpecLevel, 1.f);

	dirLS = mul(dir, (half3x3)worldToLocal1);
	furthestPlane = max(unitary1 / dirLS, negUnitary1 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS1 + dir * distance;
	output.side2.xyz += texCUBElod(blend1, half4(intersectPositionWS - cubeMapPos1, mipmapLevel)).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max(unitary2 / dirLS, negUnitary2 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	output.side2.xyz += texCUBElod(blend2, half4(intersectPositionWS - cubeMapPos2, mipmapLevel)).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max(unitary3 / dirLS, negUnitary3 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	output.side2.xyz += texCUBElod(blend3, half4(intersectPositionWS - cubeMapPos3, mipmapLevel)).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max(unitary4 / dirLS, negUnitary4 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	output.side2.xyz += texCUBElod(blend4, half4(intersectPositionWS - cubeMapPos4, mipmapLevel)).xyz * blend4Level;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	dir = half3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dir3Matrix).xyz);
	output.side3 = half4(texCUBElod(skyboxSpec, half4(dir, mipmapLevel)).xyz * skyboxSpecLevel, 1.f);

	dirLS = mul(dir, (half3x3)worldToLocal1);
	furthestPlane = max(unitary1 / dirLS, negUnitary1 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS1 + dir * distance;
	output.side3.xyz += texCUBElod(blend1, half4(intersectPositionWS - cubeMapPos1, mipmapLevel)).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max(unitary2 / dirLS, negUnitary2 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	output.side3.xyz += texCUBElod(blend2, half4(intersectPositionWS - cubeMapPos2, mipmapLevel)).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max(unitary3 / dirLS, negUnitary3 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	output.side3.xyz += texCUBElod(blend3, half4(intersectPositionWS - cubeMapPos3, mipmapLevel)).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max(unitary4 / dirLS, negUnitary4 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	output.side3.xyz += texCUBElod(blend4, half4(intersectPositionWS - cubeMapPos4, mipmapLevel)).xyz * blend4Level;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	dir = float3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dir4Matrix).xyz);
	output.side4 = half4(texCUBElod(skyboxSpec, half4(dir, mipmapLevel)).xyz * skyboxSpecLevel, 1.f);

	dirLS = mul(dir, (half3x3)worldToLocal1);
	furthestPlane = max(unitary1 / dirLS, negUnitary1 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS1 + dir * distance;
	output.side4.xyz += texCUBElod(blend1, half4(intersectPositionWS - cubeMapPos1, mipmapLevel)).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max(unitary2 / dirLS, negUnitary2 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	output.side4.xyz += texCUBElod(blend2, half4(intersectPositionWS - cubeMapPos2, mipmapLevel)).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max(unitary3 / dirLS, negUnitary3 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	output.side4.xyz += texCUBElod(blend3, half4(intersectPositionWS - cubeMapPos3, mipmapLevel)).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max(unitary4 / dirLS, negUnitary4 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	output.side4.xyz += texCUBElod(blend4, half4(intersectPositionWS - cubeMapPos4, mipmapLevel)).xyz * blend4Level;

	return output;
}
PSO PS1(PSI input) : COLOR0
{
	PSO output;

	half3 unitary1 = 1.f - reflCameraLS1;
	half3 unitary2 = 1.f - reflCameraLS2;
	half3 unitary3 = 1.f - reflCameraLS3;
	half3 unitary4 = 1.f - reflCameraLS4;

	half3 negUnitary1 = -1.f - reflCameraLS1;
	half3 negUnitary2 = -1.f - reflCameraLS2;
	half3 negUnitary3 = -1.f - reflCameraLS3;
	half3 negUnitary4 = -1.f - reflCameraLS4;

	/////////////////////////////////////////////////////////////////////////////////////////////
	half3 dir = half3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dir1Matrix).xyz);
	output.side1 = half4(texCUBElod(skyboxSpec, half4(dir, mipmapLevel)).xyz * skyboxSpecLevel, 1.f);

	half3 dirLS = mul(dir, (half3x3)worldToLocal1);
	half3 furthestPlane = max(unitary1 / dirLS, negUnitary1 / dirLS);
	float distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	half3 intersectPositionWS = reflCameraWS1 + dir * distance;
	output.side1.xyz += texCUBElod(blend1, half4(intersectPositionWS - cubeMapPos1, mipmapLevel)).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max(unitary2 / dirLS, negUnitary2 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	output.side1.xyz += texCUBElod(blend2, half4(intersectPositionWS - cubeMapPos2, mipmapLevel)).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max(unitary3 / dirLS, negUnitary3 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	output.side1.xyz += texCUBElod(blend3, half4(intersectPositionWS - cubeMapPos3, mipmapLevel)).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max(unitary4 / dirLS, negUnitary4 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	output.side1.xyz += texCUBElod(blend4, half4(intersectPositionWS - cubeMapPos4, mipmapLevel)).xyz * blend4Level;

	///////////////////////////////////////////////////////////////////////////////////////////
	dir = half3(input.uv.x, 1.f - input.uv.y, 1.0f);
	dir = mad(dir, 2.0f, -1.0f);
	dir = normalize(mul(float4(dir, 0.f), dir2Matrix).xyz);
	output.side2 = half4(texCUBElod(skyboxSpec, half4(dir, mipmapLevel)).xyz * skyboxSpecLevel, 1.f);

	dirLS = mul(dir, (half3x3)worldToLocal1);
	furthestPlane = max(unitary1 / dirLS, negUnitary1 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS1 + dir * distance;
	output.side2.xyz += texCUBElod(blend1, half4(intersectPositionWS - cubeMapPos1, mipmapLevel)).xyz * blend1Level;

	dirLS = mul(dir, (half3x3)worldToLocal2);
	furthestPlane = max(unitary2 / dirLS, negUnitary2 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS2 + dir * distance;
	output.side2.xyz += texCUBElod(blend2, half4(intersectPositionWS - cubeMapPos2, mipmapLevel)).xyz * blend2Level;

	dirLS = mul(dir, (half3x3)worldToLocal3);
	furthestPlane = max(unitary3 / dirLS, negUnitary3 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS3 + dir * distance;
	output.side2.xyz += texCUBElod(blend3, half4(intersectPositionWS - cubeMapPos3, mipmapLevel)).xyz * blend3Level;

	dirLS = mul(dir, (half3x3)worldToLocal4);
	furthestPlane = max(unitary4 / dirLS, negUnitary4 / dirLS);
	distance = min(furthestPlane.x, min(furthestPlane.y, furthestPlane.z));
	intersectPositionWS = reflCameraWS4 + dir * distance;
	output.side2.xyz += texCUBElod(blend4, half4(intersectPositionWS - cubeMapPos4, mipmapLevel)).xyz * blend4Level;

	output.side3 = 0.f;
	output.side4 = 0.f;

	return output;
}
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS0();
	}
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS1();
	}
};
