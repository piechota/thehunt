texture skyboxCubeTex;
samplerCUBE skyboxCube = sampler_state
{
	texture = < skyboxCubeTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

float4x4 mat;
float roughness;

struct VSIO
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

VSIO VS(VSIO input)
{
	VSIO output;

	output.position = input.position;
	output.uv = input.uv;

	return output;
}
static const float PI = 3.14159265358979f;

float3 ImportanceSampleGGX(float3 n, float2 xi, float roughness)
{
	float a = roughness * roughness;
	float phi = 2 * PI * xi.x;
	float cosTheta = sqrt((1.0f - xi.y) / (1.0f + (a * a - 1.0f) * xi.y));
	float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

	float3 h;
	h.x = sinTheta * cos(phi);
	h.y = sinTheta * sin(phi);
	h.z = cosTheta;

	float3 upVector = abs(n.z) < 0.999f ? float3(0.0f, 0.0f, 1.0f) : float3(1.0f, 0.0f, 0.0f);
	float3 tangentX = normalize(cross(upVector, n));
	float3 tangentY = cross(n, tangentX);

	return mad(tangentX, h.x, mad(tangentY, h.y, n * h.z));
}
float3 PrefilterEnvMap(float3 r, float roughness)
{
	float3 n = r;
	float3 v = r;

	float3 prefilteredColor = 0.0f;
	float totalWeight = 0.0f;

	const float numSamples = 128.f;
	for (float x = 0.f; x < numSamples; ++x)
	{
		for (float y = 0.f; y < numSamples; ++y)
		{
			float2 xi = float2(x / numSamples, y / numSamples);
			float3 h = ImportanceSampleGGX(n, xi, roughness);
			float3 l = 2 * dot(v, h) * h - v;

			float NoL = max(0.0f, dot(n, l));
			if (NoL > 0.0f)
			{
				prefilteredColor += texCUBE(skyboxCube, l).rgb * NoL;
				totalWeight += NoL;
			}
		}
	}

	return prefilteredColor / totalWeight;
}

float4 PS(VSIO input) : COLOR0
{
	float3 color = 0.f;

	float3 normal = float3(input.uv.x, 1.f - input.uv.y, 1.0f);
	normal = mad(normal, 2.0f, -1.0f);
	normal = normalize(mul(float4(normal, 0.f), mat).xyz);

	return float4(PrefilterEnvMap(normal, roughness), 1.f);
}
technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};