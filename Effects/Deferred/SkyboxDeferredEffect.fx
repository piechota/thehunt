texture skyboxMap : register(t9);
sampler skyboxSampler : register(s9)
= sampler_state
{
	texture = < skyboxMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
float4x4 vpMatrix;

struct VSIO
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

VSIO VS(VSIO input)
{
	VSIO output;

	output.position = mul(input.position, vpMatrix);
	output.uv = input.uv;

	return output;
}
struct PSO
{
	float4 albedo : COLOR0;
	float4 normal_metalness : COLOR1;
	float4 emissive_roughness : COLOR2;
	float4 depth : COLOR3;
};
PSO PS(VSIO input)
{
	PSO output;
	output.albedo = float4(tex2D(skyboxSampler, input.uv).rgb, 0.0f);
	output.albedo.xyz = dot(output.albedo.xyz, float3(0.3f, 0.6f, 0.1f));
	output.normal_metalness = 0.f;
	output.normal_metalness.xyz = 0.5f;
	output.emissive_roughness = 0.f;
	output.depth = 1.f;

	return output;
}

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};