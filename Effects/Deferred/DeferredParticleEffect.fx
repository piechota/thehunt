texture diffuse : register(t9);
sampler diffuseSampler : register(s9)
= sampler_state
{
	texture = < diffuse >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
float4x4 VP;
float ar; //aspect ratio

struct VSI
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

struct VSO
{
	float4 position : POSITION0;
	float4 instanceColor : COLOR0;
	float2 uv : TEXCOORD0;
	float2 depth : TEXCOORD1;
};

struct PSO
{
	float4 albedo : COLOR0;
	float4 normal_metalness : COLOR1;
	float4 emissive_roughness : COLOR2;
	float4 depth : COLOR3;
};
//TEXCOORD1 (float3 pos, float size), TEXCOORD2 (float3 color, float alpha), TEXCOORD3 (rotation matrix 2x2)
VSO VS(VSI input, float4 pos : TEXCOORD1, float4 color : TEXCOORD2, float4 rotation : TEXCOORD3)
{
	VSO output;

	//color
	output.instanceColor = color;

	//position
	input.position.xy = mul(input.position, (float2x2)rotation);
	float2 offset = float2(input.position.x, input.position.y * ar) * pos.w;

	pos.w = 1.0f;
	output.position = mul(pos, VP);

	output.position.xy += offset;
	output.depth = output.position.zw;
	//uv
	output.uv = input.uv;

	return output;
}

float3 floatToColor(float f)
{
	float3 color;
	f *= 256.f;
	color.r = floor(f);
	f = (f - color.r) * 256.f;
	color.g = floor(f);
	color.b = f - color.g;
	color.rg *= 0.00390625f;

	return color;
}

PSO PS(VSO input)
{
	PSO output;
	//output.albedo = tex2D(diffuseSampler, input.uv) * input.instanceColor.rgba;				// use this for alpha premultiplied
	output.albedo = 0.f;// tex2D(diffuseSampler, input.uv) + float4(input.instanceColor.rgb, 0);
	//output.albedo.a *= input.instanceColor.a;

	output.depth = 0.f;
	output.normal_metalness = 0.f;
	output.emissive_roughness = tex2D(diffuseSampler, input.uv) + float4(input.instanceColor.rgb, 0);
	output.emissive_roughness *= input.instanceColor.a;
	return output;
}

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};