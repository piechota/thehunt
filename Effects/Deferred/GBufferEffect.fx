texture diffuseTex : register(t9);
sampler diffuseSampler  : register(s9) 
= sampler_state
{
	texture = < diffuseTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
texture normalTex : register(t10);
sampler normalSampler  : register(s10)
 = sampler_state
{
	texture = < normalTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
texture metalnessTex : register(t11);
sampler metalnessSampler : register(s11)
= sampler_state
{
	texture = < metalnessTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
texture roughnessTex : register(t12);
sampler roughnessSampler  : register(s12)
= sampler_state
{
	texture = < roughnessTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
texture emissiveTex : register(t13);
sampler emissiveSampler  : register(s13) = sampler_state
{
	texture = < emissiveTex >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

float4x4 wvpMatrix;
float4x4 wvMatrix;
float4 uvOT;
float emissiveIntensity;

float3 floatToColor(float f)
{
	float3 color;
	f *= 256.f;
	color.r = floor(f);
	f = (f - color.r) * 256.f;
	color.g = floor(f);
	color.b = f - color.g;
	color.rg *= 0.00390625f;

	return color;
}

struct VSI
{
	float4 position : POSITION0;
	float3 tangent : TANGENT0;
	float3 bitangent : BINORMAL0;
	float3 normal : NORMAL0;
	float2 uv : TEXCOORD0;
};
struct VSO
{
	float3x3 TBN : TEXCOORD0;
	float4 position : POSITION0;
	float2 uv : TEXCOORD3;
	float2 depth : TEXCOORD4;
};
VSO VS(VSI input)
{
	VSO output;

	output.position = mul(input.position, wvpMatrix);
	output.uv = mad(input.uv, uvOT.zw, uvOT.xy);
	output.depth.xy = output.position.zw;
	output.TBN[0] = normalize(mul(float4(input.tangent, 0.f), wvMatrix).xyz);
	output.TBN[1] = normalize(mul(float4(input.bitangent, 0.f), wvMatrix).xyz);
	output.TBN[2] = normalize(mul(float4(input.normal, 0.f), wvMatrix).xyz);

	return output;
}
struct PSO
{
	float4 albedo : COLOR0;
	float4 normal_metalness : COLOR1;
	float4 emissive_roughness : COLOR2;
	float4 depth : COLOR3;
};
PSO PS(VSO input)
{
	PSO output;


	output.albedo = tex2D(diffuseSampler, input.uv).rgba;
	float d = dot(output.albedo.rgb, float3(0.3f, 0.6f, 0.1f));
	output.albedo.rgb = d;
	output.albedo.a = 0.f;

	float3 N = mad(tex2D(normalSampler, input.uv).rgb, 2.f, -1.f);
	N = mul(N, input.TBN);
	N = normalize(N);

	output.normal_metalness.xyz = mad(N, 0.5f, 0.5f);
	output.normal_metalness.a = tex2D(metalnessSampler, input.uv).r;

	output.emissive_roughness.rgb = tex2D(emissiveSampler, input.uv).rgb * emissiveIntensity;
	output.emissive_roughness.a = pow(1.f - tex2D(roughnessSampler, input.uv).r, 4);

	output.depth.rgb = floatToColor(input.depth.x / input.depth.y);
	output.depth.a = 0.0f;

	return output;
}

technique Default0
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};