texture skyboxDiffCUBE : register(t0);
samplerCUBE skyboxDiff  : register(s0)
= sampler_state
{
	texture = < skyboxDiffCUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture skyboxSpecCUBE : register(t1);
samplerCUBE skyboxSpec  : register(s1)
= sampler_state
{
	texture = < skyboxSpecCUBE >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture diffEnvCube : register(t2);
samplerCUBE diffEnvCubeSampler  : register(s2) 
= sampler_state
{
	texture = < diffEnvCube >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture specEnvCube : register(t3);
samplerCUBE specEnvCubeSampler  : register(s3)
= sampler_state
{
	texture = < specEnvCube >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture intText : register(t8);
sampler intSampler : register(s8)
= sampler_state
{
	texture = < intText >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

texture albedoTex : register(t4);
sampler albedo	 : register(s4)
= sampler_state
{
	texture = < albedoTex >;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture normal_metalnessTex : register(t5);
sampler normal_metalness : register(s5)
= sampler_state
{
	texture = < normal_metalnessTex >;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture emissive_roughnessTex : register(t6);
sampler emissive_roughness : register(s6)
= sampler_state
{
	texture = < emissive_roughnessTex >;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
texture depthTex : register(t7);
sampler depth : register(s7)
= sampler_state
{
	texture = < depthTex >;
	MINFILTER = POINT;
	MAGFILTER = POINT;
	MIPFILTER = POINT;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

struct DirectionalLight
{
	float3 forward;
	float3 color;
	float intensity;
};
DirectionalLight directionalLight;
struct PointLight
{
	float3 color;
	float3 position;
	float radius;
	float intensity;
};
PointLight pLight0;
PointLight pLight1;
PointLight pLight2;
PointLight pLight3;
PointLight pLight4;
PointLight pLight5;

float4x4 pMatrixInvert;
float4x4 vMatrixInvert;
static const float2 HELP = float2(3.14159265359f, 0.00000000001f);
static const float3 byte_to_float = float3(1.0f, 1.0f / 256.f, 1.0f / (256.f * 256.f));

struct VSIO
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

VSIO VS(VSIO input)
{
	VSIO output;

	output.position = input.position;
	output.uv = input.uv;

	return output;
}

float4 dPS(VSIO input) : COLOR0
{
	float3 depth_test = tex2D(depth, input.uv).rgb;
	clip(dot(depth_test, 1.f) >= 3.f ? -1.f : 1.f);

	float3 base_color = tex2D(albedo, input.uv).rgb;
	float m = tex2D(normal_metalness, input.uv).a;
	float roughness = tex2D(emissive_roughness, input.uv).a;
	float3 f0 = max(m * base_color, 0.045f);
	float3 N = mad(tex2D(normal_metalness, input.uv).xyz, 2.f, -1.f);
	N = normalize(N);

	float4 V = 1.f;
	V.xy = input.uv;
	V.y = 1.f - V.y;
	V.xy = mad(V.xy, 2.f, -1.f);
	V.z = dot(depth_test, byte_to_float);
	V = mul(V, pMatrixInvert);
	V.xyz /= V.w;
	V.xyz = normalize(-V.xyz);

	float3 L = -directionalLight.forward;
	float3 H = normalize(V.xyz + L);

	float4 dots;
	dots.x = dot(V.xyz, H);
	dots.y = dot(N, H);
	dots.z = dot(N, V.xyz);
	dots.w = dot(N, L);
	dots.xyzw = saturate(dots.xyzw);

	float a = roughness + 1.f;
	float roughness4 = pow(roughness, 4.f);

	float2 k = (a * a) / 8.f;
	k.x = 1.f - k.x;
	float g1y = dots.z / mad(dots.z, k.x, k.y);

	float p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

	return float4(
		(
		(base_color * (1.f - m))  + 
		(((roughness4 / (HELP.x * p * p))
		* (dots.w / mad(dots.w, k.x, k.y)) * g1y
		* mad(pow(1.0f - dots.x, 5.f), 1.0f - f0, f0))
		/ (4.f * dots.w * dots.y))
		) * 
		directionalLight.color * directionalLight.intensity * dots.w,
		1.f);
}
float4 sPS(VSIO input) : COLOR0
{
	float3 depth_test = tex2D(depth, input.uv).rgb;
	clip(dot(depth_test, 1.f) < 3.f ? -1.f : 1.f);

	return tex2D(albedo, input.uv);
}

float4 inPS(VSIO input) : COLOR0
{
	float3 depth_test = tex2D(depth, input.uv).rgb;
	if(dot(depth_test, 1.f) >= 3.f) return tex2D(emissive_roughness, input.uv);

	float3 base_color = tex2D(albedo, input.uv).rgb;
	float roughness = tex2D(emissive_roughness, input.uv).a;
	float3 N = mad(tex2D(normal_metalness, input.uv).xyz, 2.f, -1.f);
	N = normalize(N);

	float4 V = 1.f;
	V.xy = input.uv;
	V.y = 1.f - V.y;
	V.xy = mad(V.xy, 2.f, -1.f);
	V.z = dot(depth_test, byte_to_float);
	V = mul(V, pMatrixInvert);
	V.xyz /= V.w;
	V.xyz = normalize(-(V.xyz /= V.w));

	float3 wR = -reflect(V.xyz, N);
	wR = normalize(mul(float4(wR, 0.f), vMatrixInvert).xyz);

	float m = tex2D(normal_metalness, input.uv).a;

	float NoU = step(0.2f, dot(N, float3(0.f, 1.f, 0.f)));

	float3 prefilteredColor = (texCUBE(specEnvCubeSampler, wR).rgb * NoU + (1.f - NoU) * texCUBE(skyboxSpec, wR).rgb) * (.091f - 0.09f * roughness) / (roughness + 0.1f);
	float2 envBRDF = tex2D(intSampler, float2(saturate(dot(N, V)), 1.0f - roughness)).xy;

	return float4(
		mad(
		(base_color * (1.f - m)),
		//texCUBE(diffEnvCubeSampler, wR).rgb,
		texCUBE(skyboxDiff, wR).rgb,
		prefilteredColor * mad(max(m * base_color, 0.045f), envBRDF.x, envBRDF.y) +
		tex2D(emissive_roughness, input.uv)),
		1.f);
}

float4 emiPS(VSIO input) : COLOR0
{
	float3 depth_test = tex2D(depth, input.uv).rgb;
	if (dot(depth_test, 1.f) >= 3.f) return float4(tex2D(emissive_roughness, input.uv).rgb, 1.f);

	float3 base_color = tex2D(albedo, input.uv).rgb;
		float roughness = tex2D(emissive_roughness, input.uv).a;
	float3 N = mad(tex2D(normal_metalness, input.uv).xyz, 2.f, -1.f);
		N = normalize(N);

	float4 V = 1.f;
		V.xy = input.uv;
	V.y = 1.f - V.y;
	V.xy = mad(V.xy, 2.f, -1.f);
	V.z = dot(depth_test, byte_to_float);
	V = mul(V, pMatrixInvert);
	V.xyz /= V.w;
	V.xyz = normalize(-(V.xyz /= V.w));

	float3 wR = -reflect(V.xyz, N);
		wR = normalize(mul(float4(wR, 0.f), vMatrixInvert).xyz);

	float m = tex2D(normal_metalness, input.uv).a;

	//return float4((base_color * (1.f - m)) + tex2D(emissive_roughness, input.uv).rgb, 1.f);
		return float4(
		mad(
		(base_color * (1.f - m)),
		texCUBE(skyboxDiff, wR).rgb,
		tex2D(emissive_roughness, input.uv)),
		1.f);
}
bool pL0i;
bool pL1i;
bool pL2i;
bool pL3i;
bool pL4i;
bool pL5i;

float4 pPS(VSIO input) : COLOR0
{
	float3 depth_test = tex2D(depth, input.uv).rgb;
	clip(dot(depth_test, 1.f) >= 3.f ? -1.f : 1.f);

	float3 base_color = tex2D(albedo, input.uv).rgb;
	float m = tex2D(normal_metalness, input.uv).a;

	float3 diffuseColor = (base_color * (1.f - m)) ;
	float3 f0 = max(m * base_color, 0.045f);
	float3 nf0 = 1.f - f0;
	float roughness = tex2D(emissive_roughness, input.uv).a;

	float3 N = mad(tex2D(normal_metalness, input.uv).xyz, 2.f, -1.f);
	N = normalize(N);

	float4 position = 1.f;
	position.xy = input.uv;
	position.y = 1.f - position.y;
	position.xy = mad(position.xy, 2.f, -1.f);
	position.z = dot(depth_test, byte_to_float);
	position = mul(position, pMatrixInvert);
	position.xyz /= position.w;

	float3 V = normalize(-position.xyz);

	float3 pL0 = 0.f;
	float3 pL1 = 0.f;
	float3 pL2 = 0.f;
	float3 pL3 = 0.f;
	float3 pL4 = 0.f;
	float3 pL5 = 0.f;
	float3 L = 0.f;
	float3 H = 0.f;
	float falloff = 0.f;
	float4 dots = 0.f;
	dots.z = saturate(dot(N, V));
	float a = roughness + 1.f;
	float p = 0.f;
	float2 k = (a * a) / 8.f;
	k.x = 1.f - k.x;
	float g1y = dots.z / mad(dots.z, k.x, k.y);
	float roughness4 = pow(roughness, 4.f);
	float n = 0.f;
	float dist;
	if (pL0i)
	{
		L = normalize(pLight0.position - position.xyz);
		H = normalize(V + L);

		dist = distance(pLight0.position, position.xyz);
		n = saturate(1.f - pow(dist / max(pLight0.radius, HELP.y), 4.f));
		falloff = (n * n) / mad(dist, dist, 1.f);

		dots.x = dot(V, H);
		dots.y = dot(N, H);
		dots.w = dot(N, L);
		dots.xyw = saturate(dots.xyw);

		p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

		pL0 = (
			diffuseColor + 
			(((roughness4 / (HELP.x * p * p))
			* (dots.w / mad(dots.w, k.x, k.y)) * g1y
			* mad(pow(1.0f - dots.x, 5.f), nf0, f0))
			/ (4.f * dots.w * dots.y))) *
			pLight0.color * pLight0.intensity * dots.w * falloff;
	}
	if (pL1i)
	{
		L = normalize(pLight1.position - position.xyz);
		H = normalize(V + L);
		dots.x = dot(V, H);
		dots.y = dot(N, H);
		dots.w = dot(N, L);
		dots.xyw = saturate(dots.xyw);

		dist = distance(pLight1.position, position.xyz);
		n = saturate(1.f - pow(dist / max(pLight1.radius, HELP.y), 4.f));
		falloff = (n * n) / mad(dist, dist, 1.f);

		p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

		pL1 = (
			diffuseColor +
			(((roughness4 / (HELP.x * p * p))
			* (dots.w / mad(dots.w, k.x, k.y)) * g1y
			* mad(pow(1.0f - dots.x, 5.f), nf0, f0))
			/ (4.f * dots.w * dots.y))) *
			pLight1.color * pLight1.intensity * dots.w * falloff;
	}
	if (pL2i)
	{
		L = normalize(pLight2.position - position.xyz);
		H = normalize(V + L);
		dots.x = dot(V, H);
		dots.y = dot(N, H);
		dots.w = dot(N, L);
		dots.xyw = saturate(dots.xyw);
		
		dist = distance(pLight2.position, position.xyz);
		n = saturate(1.f - pow(dist / max(pLight2.radius, HELP.y), 4.f));
		falloff = (n * n) / mad(dist, dist, 1.f);
		
		p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

		pL2 = (
			diffuseColor +
			(((roughness4 / (HELP.x * p * p))
			* (dots.w / mad(dots.w, k.x, k.y)) * g1y
			* mad(pow(1.0f - dots.x, 5.f), nf0, f0))
			/ (4.f * dots.w * dots.y))) *
			pLight2.color * pLight2.intensity * dots.w * falloff;
	}
	if (pL3i)
	{
		L = normalize(pLight3.position - position.xyz);
		H = normalize(V + L);
		dots.x = dot(V, H);
		dots.y = dot(N, H);
		dots.w = dot(N, L);
		dots.xyw = saturate(dots.xyw);
		
		dist = distance(pLight3.position, position.xyz);
		n = saturate(1.f - pow(dist / max(pLight3.radius, HELP.y), 4.f));
		falloff = (n * n) / mad(dist, dist, 1.f);
		
		p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

		pL3 = (
			diffuseColor +
			(((roughness4 / (HELP.x * p * p))
			* (dots.w / mad(dots.w, k.x, k.y)) * g1y
			* mad(pow(1.0f - dots.x, 5.f), nf0, f0))
			/ (4.f * dots.w * dots.y))) *
			pLight3.color * pLight3.intensity * dots.w * falloff;
	}
	if (pL4i)
	{
		L = normalize(pLight4.position - position.xyz);
		H = normalize(V + L);
		dots.x = dot(V, H);
		dots.y = dot(N, H);
		dots.w = dot(N, L);
		dots.xyw = saturate(dots.xyw);
		
		dist = distance(pLight4.position, position.xyz);
		n = saturate(1.f - pow(dist / max(pLight4.radius, HELP.y), 4.f));
		falloff = (n * n) / mad(dist, dist, 1.f);
		
		p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

		pL4 = (
			diffuseColor +
			(((roughness4 / (HELP.x * p * p))
			* (dots.w / mad(dots.w, k.x, k.y)) * g1y
			* mad(pow(1.0f - dots.x, 5.f), nf0, f0))
			/ (4.f * dots.w * dots.y))) *
			pLight4.color * pLight4.intensity * dots.w * falloff;
	}
	if (pL5i)
	{
		L = normalize(pLight5.position - position.xyz);
		H = normalize(V + L);
		dots.x = dot(V, H);
		dots.y = dot(N, H);
		dots.w = dot(N, L);
		dots.xyw = saturate(dots.xyw);

		dist = distance(pLight5.position, position.xyz);
		n = saturate(1.f - pow(dist / max(pLight5.radius, HELP.y), 4.f));
		falloff = (n * n) / mad(dist, dist, 1.f);

		p = mad(dots.y * dots.y, roughness4 - 1.f, 1.f);

		pL5 = (
			diffuseColor +
			(((roughness4 / (HELP.x * p * p))
			* (dots.w / mad(dots.w, k.x, k.y)) * g1y
			* mad(pow(1.0f - dots.x, 5.f), nf0, f0))
			/ (4.f * dots.w * dots.y))) *
			pLight5.color * pLight5.intensity * dots.w * falloff;
	}
	return float4(pL0 + pL1 + pL2 + pL3 + pL4 + pL5, 1.f);
}
technique Skybox
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 sPS();
	}
};

technique Directional
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 dPS();
	}
};

technique Point
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 pPS();
	}
};

technique Indirect
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 inPS();
	}
};

technique Emissive
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 emiPS();
	}
};