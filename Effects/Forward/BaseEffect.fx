float4x4 wvpMatrix;
float4x4 wMatrix;
float4x4 shadowMatrix;

texture diffuseMap;
texture normalMap;
texture metalnessMap;
texture roughnessMap;
texture emissivMap;

texture shadowMap;

texture diffEnvCube;
texture specEnvCube;
texture intText;

float emissiveIntensity;

samplerCUBE diffEnvCubeSampler = sampler_state
{
	texture = < diffEnvCube >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
samplerCUBE specEnvCubeSampler = sampler_state
{
	texture = < specEnvCube >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
sampler intSampler = sampler_state
{
	texture = < intText >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};
sampler diffuseSampler = sampler_state
{
	texture = < diffuseMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
sampler normalSampler = sampler_state
{
	texture = < normalMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
sampler metalnessSampler = sampler_state
{
	texture = < metalnessMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
sampler roughnessSampler = sampler_state
{
	texture = < roughnessMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};
sampler emissivSampler = sampler_state
{
	texture = < emissivMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

sampler shadowSampler = sampler_state
{
	texture = < shadowMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = WRAP;
	ADDRESSV = WRAP;
};

struct DirectionalLight
{
	float3 forward;
	float3 color;
	float intensity;
};
struct PointLight
{
	float3 color;
	float3 position;
	float radius;
	float intensity;
};

struct VSI
{
	float4 position : POSITION0;
	float3 tangent : TANGENT0;
	float3 bitangent : BINORMAL0;
	float3 normal : NORMAL0;
	float2 uv : TEXCOORD0;
};
struct VSO
{
	float3x3 TBN : TEXCOORD0;
	float4 position : POSITION0;
	float4 sPosition : TEXCOORD4;
	float3 cPosition : TEXCOORD5;
	float2 uv : TEXCOORD6;
	float2 depth : TEXCOORD7;
};

DirectionalLight directionalLight;
PointLight pointLight0;

float3 eyePosition;

const float2 HELP = float2(3.14159265359f, 0.00000000001f);
const float2 transform = float2(2.f, -1.f);
VSO VS(VSI input)
{
	VSO output;

	output.position = mul(input.position, wvpMatrix);
	output.uv = input.uv;
	output.depth.xy = output.position.zw;
	output.TBN[0] = input.tangent;
	output.TBN[1] = input.bitangent;
	output.TBN[2] = input.normal;
	output.TBN = mul(output.TBN, wMatrix);
	output.cPosition = mul(input.position, wMatrix);
	output.sPosition = mul(input.position, shadowMatrix);
	output.sPosition.xy = mad(output.sPosition.xy, 0.5f, 0.5f);
	output.sPosition.y = 1.0f - output.sPosition.y;

	return output;
}

struct PSO
{
	float4 diffuse : COLOR0;
	float4 depth : COLOR1;
};

float3 floatToColor(float f)
{
	float3 color;
	f *= 256.f;
	color.r = floor(f);
	f = (f - color.r) * 256.f;
	color.g = floor(f);
	color.b = f - color.g;
	color.rg *= 0.00390625f;

	return color;
}
float colorToFloat(float3 color)
{
	const float3 byte_to_float = float3(1.0f, 1.0f / 256.f, 1.0f / (256.f * 256.f));
		return dot(color, byte_to_float);
}
//////////////////////////////		UE4
float D(float4 dots, float roughness)
{
	float a = pow(roughness, 4.0f);
	float m = mad(dots.y * dots.y, a - 1.f, 1.f);

	return a / (HELP.x * m * m);
}
float G(float4 dots, float roughness)
{
	float a = roughness + 1.f;

	float2 k = (a * a) / 8.f;
	k.x = 1.f - k.x;
	float2 g1;
	g1.x = dots.w / mad(dots.w, k.x, k.y);
	g1.y = dots.z / mad(dots.z, k.x, k.y);

	return g1.x * g1.y;
}
float3 F(float4 dots, float3 f0)
{
	return mad(pow(1.0f - dots.x, 5.f), 1.0f - f0, f0);
}
float3 BRDF(float4 dots, float3 f0, float roughness)
{
	return (D(dots, roughness) * G(dots, roughness) * F(dots, f0)) / (4.f * dots.w * dots.y);
}
float3 ApproximateSpecular(float4 dots, float3 f0, float3 r, float roughness)
{
	float3 prefilteredColor = texCUBElod(specEnvCubeSampler, float4(r, round(roughness * 5.f))).rgb;
	float2 envBRDF = tex2D(intSampler, float2(dots.z, 1.0f - roughness)).xy;

	return prefilteredColor * mad(f0, envBRDF.x, envBRDF.y);
}
////////////////////////////////////////////////////////////////////////////
float3 calcDirectLight(float4 dots, float3 lightColor, float3 diffuseColor, float3 f0, float roughness, float lightIntensity)
{
	return (diffuseColor + BRDF(dots, f0, roughness)) * lightColor * lightIntensity * dots.w;
}
float Falloff(float distance, float radius)
{
	float n = saturate(1.f - pow(distance / max(radius, HELP.y), 4.f));
	return (n * n) / mad(distance, distance, 1.f);
}

PSO PS(VSO input)
{
	PSO output;

	float3 dif = tex2D(diffuseSampler, input.uv).rgb;
	float m = tex2D(metalnessSampler, input.uv).r;

	float3 diffuseColor = (dif * (1.0f - m)) / HELP.x;
	float3 f0 = max(m * m * dif * 0.16, m * 0.045f);
	float roughness = (1.0f - tex2D(roughnessSampler, input.uv).r);

	float3 wN = mad(tex2D(normalSampler, input.uv).rgb, transform.x, transform.y);
	wN = mul(wN, input.TBN);
	wN = normalize(wN);

	float3 wV = normalize(eyePosition - input.cPosition);
	float3 wR = -reflect(wV, wN);

	float3 wL = normalize(-directionalLight.forward);
	float3 wH = normalize(wV + wL);

	float4 dots;
	dots.x = dot(wV, wH);
	dots.y = dot(wN, wH);
	dots.z = dot(wN, wV);
	dots.w = dot(wN, wL);
	dots.yzw = saturate(dots.yzw);

	float3 dLight = calcDirectLight(dots, directionalLight.color, diffuseColor, f0, roughness, directionalLight.intensity);

	//L0
	float falloff = Falloff(distance(pointLight0.position, input.cPosition), pointLight0.radius);
	wL = normalize(pointLight0.position - input.cPosition);
	wH = normalize(wV + wL);
	dots.x = dot(wV, wH);
	dots.y = dot(wN, wH);
	dots.w = dot(wN, wL);
	dots.yw = saturate(dots.yw);
	
	float3 pLight = calcDirectLight(dots, pointLight0.color, diffuseColor, f0, roughness, pointLight0.intensity) * falloff;

	//IndirectLight calculation
	float3 indirectSpecularFactor = ApproximateSpecular(dots, f0, wR, roughness);
	float3 envDiff = texCUBE(diffEnvCubeSampler, wR).rgb;
	float3 indirectLighting = mad(diffuseColor, envDiff, indirectSpecularFactor);

	float visibility = 1.0f;
	//if (colorToFloat(tex2D(shadowSampler, input.sPosition.xy).rgb) < (input.sPosition.z / input.sPosition.w) - 0.003f)
	//	visibility = 0.0f;

	output.diffuse.rgb = mad(emissiveIntensity, tex2D(emissivSampler, input.uv).rgb, mad(visibility, dLight, pLight) + indirectLighting);
	output.diffuse.a = 1.0f;

	output.depth.rgb = floatToColor(input.depth.x / input.depth.y);
	output.depth.w = 0.0f;

	return output;
}

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};