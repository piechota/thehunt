float4x4 shadowMatrix;

struct VSO
{
	float4 position : POSITION0;
	float4 position2D : TEXCOORD0;
};

float3 floatToColor(float f)
{
	float3 color;
	f *= 256;
	color.r = floor(f);
	f = (f - color.r) * 256;
	color.g = floor(f);
	color.b = f - color.g;
	color.rg *= 0.00390625;

	return color;
}

VSO VS(float4 inPos : POSITION0)
{
	VSO output;
	output.position = mul(inPos, shadowMatrix);
	output.position2D = output.position;

	return output;
}
float4 PS(VSO input) : COLOR0
{
	float4 color;
	color.rgb = floatToColor(input.position2D.z / input.position2D.w);
	color.a = 1.0f;
	return color;
}

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};