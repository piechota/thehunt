float4x4 vpMatrix;

texture skyboxMap : register(t4);
sampler skyboxSampler : register(s4)
= sampler_state
{
	texture = < skyboxMap >;
	MINFILTER = LINEAR;
	MAGFILTER = LINEAR;
	MIPFILTER = LINEAR;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
};

struct VSIO
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
};

VSIO VS(VSIO input)
{
	VSIO output;

	output.position = mul(input.position, vpMatrix);
	output.uv = input.uv;

	return output;
}

float4 PS(VSIO input) : COLOR0
{
	return float4(tex2D(skyboxSampler, input.uv).rgb, 1.0f);
}

technique Default
{
	pass p0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
};